# 安装jar包到本地仓库

添加新的系统环境变量：MAVEN_HOME
设置其值为你安装的目录，例如      D:\java\apache-maven-3.6.0
更新PATH变量，添加变量值          %MAVEN_HOME%\bin
运行cmd，输入mvn -v

1、在当前项目的repo目录下，打开cmd窗口
2、执行下面的命令：

mvn install:install-file -Dfile=bce-java-sdk-0.10.82.jar -DgroupId=com.baidubce -DartifactId=bce-java-sdk -Dversion=0.10.82 -Dpackaging=jar
mvn install:install-file -Dfile=jpush-client-3.3.10.jar -DgroupId=cn.jpush.api -DartifactId=jpush-client -Dversion=3.3.10 -Dpackaging=jar
mvn install:install-file -Dfile=jiguang-common-1.1.4.jar -DgroupId=cn.jpush.api -DartifactId=jiguang-common -Dversion=1.1.4 -Dpackaging=jar
mvn install:install-file -Dfile=netty-all-4.1.6.Final.jar -DgroupId=io.netty -DartifactId=netty-all -Dversion=4.1.6.Final -Dpackaging=jar