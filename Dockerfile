FROM hub.baidubce.com/cnap-public/microservice-springcloud:probe1.5.0.2_centos7.1_java8
MAINTAINER BAIDU
RUN rm -f /etc/localtime & cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo "Asia/Shanghai" > /etc/timezone
RUN yum install -y lsof


COPY ./target/dictionary-1.0-SNAPSHOT.jar /home/app/lib/
# 应用启动命令
CMD ["java","-jar","/home/app/lib/dictionary-1.0-SNAPSHOT.jar"]
