package cn.cstm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

import springfox.documentation.swagger2.annotations.EnableSwagger2;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
@MapperScan("cn.cstm.**.*.mapper")
//@EnableDiscoveryClient
@EnableCaching
@EnableTransactionManagement// 开启事务
@EnableSwagger2
@EnableEncryptableProperties //启动数据库加密功能
@ServletComponentScan(basePackages={"cn.cstm.air.util"})
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        System.out.println("构建测试");
        SpringApplication.run(Application.class, args);
    }
    /**
     * 重写该方法，然后将maven配置打包方式改为war并移除嵌入式tomcat使项目能打成war部署到服务器上
     * @param application
     * @return
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
    	return builder.sources(Application.class);
    }
}
