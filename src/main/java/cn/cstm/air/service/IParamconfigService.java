package cn.cstm.air.service;

import cn.cstm.air.domain.Paramconfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author author
 * @since 2020-04-26
 */
public interface IParamconfigService extends IService<Paramconfig> {

}
