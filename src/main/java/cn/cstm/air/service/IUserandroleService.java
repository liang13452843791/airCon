package cn.cstm.air.service;

import com.baomidou.mybatisplus.extension.service.IService;

import cn.cstm.air.domain.Userandrole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author author
 * @since 2020-04-14
 */
public interface IUserandroleService extends IService<Userandrole> {

}
