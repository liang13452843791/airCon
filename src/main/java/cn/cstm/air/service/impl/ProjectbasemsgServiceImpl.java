package cn.cstm.air.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.ImmutableMap;

import cn.cstm.air.domain.Projectbasemsg;
import cn.cstm.air.domain.User;
import cn.cstm.air.domain.Userandrole;
import cn.cstm.air.mapper.ProjectbasemsgMapper;
import cn.cstm.air.mapper.UserandroleMapper;
import cn.cstm.air.service.IProjectbasemsgService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author author
 * @since 2020-04-12
 */
@Service
public class ProjectbasemsgServiceImpl extends ServiceImpl<ProjectbasemsgMapper, Projectbasemsg>
        implements IProjectbasemsgService {

    @Autowired
    private ProjectbasemsgMapper mapper;
    @Autowired
    private UserandroleMapper userroleMapper;

    @Override
    public JSONObject deleteProjectById(String id) {
    	JSONObject json = new JSONObject();
		String message = "成功";
		String state = "00000000";
		Integer res=0;
		try {
			res = mapper.deleteProjectById(id);
			if(res>0){
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}else{
				state = "99999999";
				message = "项目信息管理删除失败";
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("报错信息|"+e.getMessage());
			state = "99999999";
			message = "数据库操作失败";
			json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
		}
		return json;
    }
    
    @Override
    public JSONObject audit(JSONObject jsonObject, HttpServletRequest req, HttpServletResponse resps) {
        JSONObject object = new JSONObject();
//        HttpSession session = req.getSession();
//        User user = (User) session.getAttribute("user");
        if (!hasAdminPower(req.getHeader("user"))) {
            object.put("audit", false);
            object.put("errorMassage", "用户无权限");
            return object;
        }
//        QueryWrapper<Userandrole> queryWrapper = new QueryWrapper<Userandrole>();
//        queryWrapper.eq("username", user.getUserName());
//        List<Object> roles = userroleMapper.selectObjs(queryWrapper);

        String projectNumber = (String) jsonObject.get("projectNumber");
        String auditStatus = (String) jsonObject.get("auditStatus");
        String des = (String) jsonObject.get("des");
        String regex = "^[A-Za-z0-9\u4e00-\u9fa5]+$";
        if (StringUtils.isNotBlank(des) && !des.matches(regex)) {
            object.put("audit", false);
            object.put("errorMassage", "原因描述必须为汉字数字");
            return object;
        }
        if (StringUtils.isNotBlank(des) && des.length() > 356) {
            object.put("audit", false);
            object.put("errorMassage", "原因描述字符长度超过356");
            return object;
        }
        
        QueryWrapper<Projectbasemsg> wrapper = new QueryWrapper();
        wrapper.eq("ProjectNumber", projectNumber);
        Projectbasemsg projectbasemsg = mapper.selectOne(wrapper);

        if (null == projectbasemsg) {
            object.put("audit", false);
            object.put("errorMassage", "无此数据");
            return object;
        }
        // 通过
        if ("审核通过".equals(auditStatus)) {
            if (!"待审核".equals(projectbasemsg.getReverse1())) {
                object.put("audit", false);
                object.put("errorMassage", "必须为待审核数据");
                return object;
            }
            mapper.update(projectbasemsg, new UpdateWrapper<Projectbasemsg>().eq("ProjectNumber", projectNumber)
                    .set("reverse1", "审核通过").set("updateTime", LocalDateTime.now()));

        }
        // 重申
        if ("重审".equals(auditStatus)) {
        	if (!"审核通过".equals(projectbasemsg.getReverse1())) {
                object.put("audit", false);
                object.put("errorMassage", "待审核数据不能重审");
                return object;
            }
            mapper.update(projectbasemsg, new UpdateWrapper<Projectbasemsg>().eq("ProjectNumber", projectNumber)
                    .set("reverse1", "待审核").set("reverse2", des).set("updateTime", LocalDateTime.now()));
        }
        // 驳回

        if ("驳回".equals(auditStatus)) {
            if (!"审核通过".equals(projectbasemsg.getReverse1())) {
                object.put("audit", false);
                object.put("errorMassage", "不是审核通过数据无法驳回");
                return object;
            }
            mapper.update(projectbasemsg, new UpdateWrapper<Projectbasemsg>().eq("ProjectNumber", projectNumber)
                    .set("reverse1", "驳回").set("reverse2", des).set("updateTime", LocalDateTime.now()));
        }
        object.put("audit", true);
        object.put("errorMassage", "成功");
        return object;
    }

    @Override
    public JSONObject banchAudit(JSONObject jsonObject, HttpServletRequest req, HttpServletResponse resps) {
        // TODO Auto-generated method stub
        JSONObject object = new JSONObject();
//        HttpSession session = req.getSession();
//        User user = (User) session.getAttribute("user");
        
        if (!hasAdminPower(req.getHeader("user"))) {
            object.put("audit", false);
            object.put("errorMassage", "用户无权限");
            return object;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String startDatestr = (String) jsonObject.get("startDate");
        String endDatestr = (String) jsonObject.get("endDate");
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = sdf.parse(startDatestr);
            endDate = sdf.parse(endDatestr);
        } catch (ParseException e) {
            e.printStackTrace();
            object.put("banchAudit", false);
            object.put("errorMassage", "日期转化异常");
            return object;
        }

        String auditStatus = (String) jsonObject.get("auditStatus");

        try {
        	if ("审核通过".equals(auditStatus)) {
        		mapper.banchUpdate(startDate, endDate, "待审核", "审核通过", new Date());
        	}else if ("驳回".equals(auditStatus)) {
        		mapper.banchUpdate(startDate, endDate, "待审核", "驳回", new Date());
        	}else {
        		object.put("banchAudit", false);
                object.put("errorMassage", "待审核数据不能批量重审");
                return object;
        	}
        } catch (Exception e) {
            e.printStackTrace();
            object.put("banchAudit", false);
            object.put("errorMassage", "数据库操作异常");
            return object;
        }
        object.put("banchAudit", true);
        object.put("errorMassage", "成功");
        return object;
    }

    @Override
    public boolean hasAdminPower(String username) {

        List<Userandrole> roles = userroleMapper.getByUsername(username);

        for (Userandrole role : roles) {
            if ("01".equals(role.getRoleID())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<LinkedHashMap<String, Object>> selectProjectCommsgList(String Reverse1, String ProjectType, String ProjectAddress, String AgencyName, String ProjectName, Date startDate, Date endDate, List<String> agencyNames) {
        return mapper.selectProjectCommsgList(Reverse1, ProjectType, ProjectAddress, AgencyName, ProjectName, startDate, endDate, agencyNames);
    }

    @Override
    public List<LinkedHashMap<String, Object>> selectProjectFammsgList(String Reverse1, String ProjectType, String ProjectAddress, String AgencyName, String CompoundName, Date startDate, Date endDate, List<String> agencyNames) {
        return mapper.selectProjectFammsgList(Reverse1, ProjectType, ProjectAddress, AgencyName, CompoundName, startDate, endDate, agencyNames);
    }

}
