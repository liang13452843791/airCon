package cn.cstm.air.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.ImmutableMap;

import cn.cstm.air.domain.AgencyMsg;
import cn.cstm.air.domain.User;
import cn.cstm.air.exception.ServiceException;
import cn.cstm.air.mapper.AgencyMsgDao;
import cn.cstm.air.mapper.UserDao;
import cn.cstm.air.service.IUserService;
import cn.cstm.air.util.ValueOfMD5;

@Service
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements IUserService{

	@Autowired
	private UserDao userDao;
	@Autowired
	private AgencyMsgDao agencyMsgDao;

	@Override
	@Transactional
	public JSONObject update(String password,String password1,String password2,String username) {
		JSONObject json = new JSONObject();
		String message = "成功";
		String state = "00000000";
		Integer res=0;
		try {
			User user = userDao.getByUserName(username);
			if(user.getPassWord().equals(password)) {
				res = userDao.updatePassword(username,ValueOfMD5.string2MD5(password1),new Date());
				if(res>0){
					json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
				}else{
					state = "99999999";
					message = "密码修改失败";
					json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
				}
			}else {
				state = "99999999";
				message = "修改失败,输入的原密码有误";
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("报错信息|"+e.getMessage());
			state = "99999999";
			message = "数据库操作失败";
			json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
		}
		return json;
	}

	@Override
	public JSONObject insertAgencyMsg(String username, String agencyname, String agencyphone, String loginusername,
			String loginuserphone) {
		JSONObject json = new JSONObject();
		String message = "成功";
		String state = "00000000";
		Integer res=0;
		try {
			AgencyMsg agencyMsg = new AgencyMsg();
			String uuid = UUID.randomUUID().toString(); 
			String id = uuid.replaceAll("-", "");
			agencyMsg.setId(id);
			agencyMsg.setAgencyName(agencyname);
			agencyMsg.setAgencyPhone(agencyphone);
			agencyMsg.setLoginUserName(loginusername);
			agencyMsg.setLoginUserPhone(loginuserphone);
			agencyMsg.setCreateTime(new Date());
			agencyMsg.setStates("1");
			agencyMsg.setUserName(username);
			res = agencyMsgDao.insertSelective(agencyMsg);
			if(res>0){
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}else{
				state = "99999999";
				message = "经销商新增失败";
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("报错信息|"+e.getMessage());
			state = "99999999";
			message = "数据库操作失败";
			json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
		}
		return json;
	}

	@Override
	public JSONObject updateAgencyMsgById(String id, String agencyname, String agencyphone, String loginusername,
			String loginuserphone) {
		JSONObject json = new JSONObject();
		String message = "成功";
		String state = "00000000";
		Integer res=0;
		try {
			AgencyMsg agencyMsg = new AgencyMsg(agencyphone,id,agencyname,loginusername,loginuserphone,new Date());
			res = agencyMsgDao.updateByPrimaryKeySelective(agencyMsg);
			if(res>0){
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}else{
				state = "99999999";
				message = "经销商修改失败";
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("报错信息|"+e.getMessage());
			state = "99999999";
			message = "数据库操作失败";
			json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
		}
		return json;
	}
	
	@Override
	public JSONObject deleteAgencyMsgById(String id) {
		JSONObject json = new JSONObject();
		String message = "成功";
		String state = "00000000";
		Integer res=0;
		try {
			res = agencyMsgDao.deleteAgencyMsgById(id);
			if(res>0){
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}else{
				state = "99999999";
				message = "经销商删除失败";
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("报错信息|"+e.getMessage());
			state = "99999999";
			message = "数据库操作失败";
			json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
		}
		return json;
	}
	
	@Override
	public JSONObject selectByAgencyName(String agencyName) {
		JSONObject json = new JSONObject();
		String message = "成功";
		String state = "00000000";
		try {
			AgencyMsg agencyMsg= agencyMsgDao.selectByAgencyName(agencyName);
			if(null!=agencyMsg) {
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
				json.put("agencyMsg",agencyMsg);
			}else {
				state = "00000001";
				message = "无数据";
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("报错信息|"+e.getMessage());
			state = "99999999";
			message = "数据库操作失败";
			json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
		}
		return json;
	}
	
	@Override
	public JSONObject selectListByAgencyName(String agencyName) {
		JSONObject json = new JSONObject();
		String message = "成功";
		String state = "00000000";
		try {
			List<AgencyMsg> list= agencyMsgDao.selectListByAgencyName(agencyName);
			if(null!=list&&list.size()>0) {
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
				json.put("list",list);
			}else {
				state = "00000001";
				message = "无数据";
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("报错信息|"+e.getMessage());
			state = "99999999";
			message = "数据库操作失败";
			json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
		}
		return json;
	}
	
	@Override
	public JSONObject selectLikeByAgencyName(String agencyName) {
		JSONObject json = new JSONObject();
		String message = "成功";
		String state = "00000000";
		try {
			List<AgencyMsg> list= agencyMsgDao.selectLikeByAgencyName(agencyName);
			if(null!=list&&list.size()>0) {
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
				json.put("list",list);
			}else {
				state = "00000001";
				message = "无数据";
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("报错信息|"+e.getMessage());
			state = "99999999";
			message = "数据库操作失败";
			json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
		}
		return json;
	}
	
	@Override
	public JSONObject selectAgencyListByAgencyNameAndUserName(String username, String agencyName) {
		JSONObject json = new JSONObject();
		String message = "成功";
		String state = "00000000";
		try {
			List<AgencyMsg> list= agencyMsgDao.selectAgencyListByAgencyNameAndUserName(username,agencyName);
			if(null!=list&&list.size()>0) {
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
				json.put("list",list);
			}else {
				state = "00000001";
				message = "无数据";
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("报错信息|"+e.getMessage());
			state = "99999999";
			message = "数据库操作失败";
			json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
		}
		return json;
	}
	
	@Override
	public JSONObject selectAgencyListByUserName(String username) {
		JSONObject json = new JSONObject();
		String message = "成功";
		String state = "00000000";
		try {
			List<AgencyMsg> list= agencyMsgDao.selectAgencyListByUserName(username);
			if(null!=list&&list.size()>0) {
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
				json.put("list",list);
			}else {
				state = "00000001";
				message = "无数据";
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("报错信息|"+e.getMessage());
			state = "99999999";
			message = "数据库操作失败";
			json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
		}
		return json;
	}
	
	@Override
	public JSONObject selectAgencyList() {
		JSONObject json = new JSONObject();
		String message = "成功";
		String state = "00000000";
		try {
			List<AgencyMsg> list= agencyMsgDao.selectAgencyList();
			if(null!=list&&list.size()>0) {
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
				json.put("list",list);
			}else {
				state = "00000001";
				message = "无数据";
				json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("报错信息|"+e.getMessage());
			state = "99999999";
			message = "数据库操作失败";
			json.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message));
		}
		return json;
	}
	
	@Override
	public List<AgencyMsg> selectAgencyNameByUserName(String username) {
		List<AgencyMsg> list= new ArrayList<AgencyMsg>();
		try {
			list= agencyMsgDao.selectAgencyNameByUserName(username);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("报错信息|"+e.getMessage());
		}
		return list;
	}
	
	@Override
	public void checkAgencyMsg(String username, String agencyname, String agencyphone, String loginusername,
			String loginuserphone) throws ServiceException {
		if(StringUtils.isBlank(username)) {
			throw new ServiceException("99999999", "用户名不能为空");
		}
		if(StringUtils.isBlank(agencyname)) {
			throw new ServiceException("99999999", "经销商名称不能为空");
		}
		if(StringUtils.isBlank(agencyphone)) {
			throw new ServiceException("99999999", "经销商电话不能为空");
		}
		if(StringUtils.isBlank(loginusername)) {
			throw new ServiceException("99999999", "登录人姓名不能为空");
		}
		if(StringUtils.isBlank(loginuserphone)) {
			throw new ServiceException("99999999", "登录人电话不能为空");
		}
	}

	@Override
	public void checkUser(String password, String password1, String password2, String username) throws ServiceException {
		if(StringUtils.isBlank(password)) {
			throw new ServiceException("99999999", "原密码不能为空");
		}
		if(StringUtils.isBlank(password1)) {
			throw new ServiceException("99999999", "新密码不能为空");
		}
		if(StringUtils.isBlank(password2)) {
			throw new ServiceException("99999999", "新密码不能为空");
		}
		if(StringUtils.isBlank(username)) {
			throw new ServiceException("99999999", "用户名不能为空");
		}
	}

	@Override
	public void checkUpdateAgencyMsg(String id, String agencyname, String agencyphone, String loginusername,
			String loginuserphone) throws ServiceException {
		if(StringUtils.isBlank(id)) {
			throw new ServiceException("99999999", "id不能为空");
		}
		if(StringUtils.isBlank(agencyname)) {
			throw new ServiceException("99999999", "经销商名称不能为空");
		}
		if(StringUtils.isBlank(agencyphone)) {
			throw new ServiceException("99999999", "经销商电话不能为空");
		}
		if(StringUtils.isBlank(loginusername)) {
			throw new ServiceException("99999999", "登录人姓名不能为空");
		}
		if(StringUtils.isBlank(loginuserphone)) {
			throw new ServiceException("99999999", "登录人电话不能为空");
		}
	}

	@Override
	public void checkUpdateAgencyMsg(String agencyname) throws ServiceException {
		if(StringUtils.isBlank(agencyname)) {
			throw new ServiceException("99999999", "经销商名称不能为空");
		}
	}

	@Override
	public void check(String agencyname, String username) throws ServiceException {
		if(StringUtils.isBlank(agencyname)) {
			throw new ServiceException("99999999", "经销商名称不能为空");
		}
		if(StringUtils.isBlank(username)) {
			throw new ServiceException("99999999", "用户名不能为空");
		}
	}

	@Override
	public void checkId(String id) throws ServiceException {
		if(StringUtils.isBlank(id)) {
			throw new ServiceException("99999999", "id不能为空");
		}
	}

}
