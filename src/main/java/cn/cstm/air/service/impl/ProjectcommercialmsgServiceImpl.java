package cn.cstm.air.service.impl;

import cn.cstm.air.domain.Projectcommercialmsg;
import cn.cstm.air.mapper.ProjectcommercialmsgMapper;
import cn.cstm.air.service.IProjectcommercialmsgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author author
 * @since 2020-04-17
 */
@Service
public class ProjectcommercialmsgServiceImpl extends ServiceImpl<ProjectcommercialmsgMapper, Projectcommercialmsg> implements IProjectcommercialmsgService {

}
