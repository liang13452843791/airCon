package cn.cstm.air.service.impl;

import cn.cstm.air.domain.Projectfamilymsg;
import cn.cstm.air.mapper.ProjectfamilymsgMapper;
import cn.cstm.air.service.IProjectfamilymsgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author author
 * @since 2020-04-17
 */
@Service
public class ProjectfamilymsgServiceImpl extends ServiceImpl<ProjectfamilymsgMapper, Projectfamilymsg> implements IProjectfamilymsgService {

}
