package cn.cstm.air.service.impl;

import cn.cstm.air.domain.Parameter;
import cn.cstm.air.mapper.ParameterMapper;
import cn.cstm.air.service.IParameterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author author
 * @since 2020-04-26
 */
@Service
public class ParameterServiceImpl extends ServiceImpl<ParameterMapper, Parameter> implements IParameterService {

}
