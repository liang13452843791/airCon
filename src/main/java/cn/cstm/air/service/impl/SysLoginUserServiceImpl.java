package cn.cstm.air.service.impl;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import cn.cstm.air.domain.User;
import cn.cstm.air.mapper.UserDao;
import cn.cstm.air.service.SysLoginUserService;

@Service
public class SysLoginUserServiceImpl implements SysLoginUserService {
	@Autowired
	private UserDao userDao;

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public JSONObject loginAction(JSONObject jsonObject, HttpServletRequest req, HttpServletResponse resps) {
		JSONObject map = new JSONObject();
		String username = StringUtils.isBlank((String) jsonObject.get("username")) ? ""
				: (String) jsonObject.get("username");
		String password = StringUtils.isBlank((String) jsonObject.get("password")) ? ""
				: (String) jsonObject.get("password");
		String code = StringUtils.isBlank((String) jsonObject.get("code")) ? "" : (String) jsonObject.get("code");
		HttpSession session = req.getSession();
		
//		Enumeration<String> attributeNames = session.getAttributeNames();
		
		logger.info("登录接口获取session:"+session.toString());
		logger.info("登录接口获取session----------------begin----");
//		while (attributeNames.hasMoreElements()) {
//			String attributeName = (String) attributeNames.nextElement();
//			Object attribute = session.getAttribute(attributeName);
//			logger.info("登录接口获取session属性"+attributeName+"，对应值为："+attribute.toString());
//		}
//		logger.info("登录接口获取session----------------end----");
		
		String sessionCode = req.getHeader("Cookie");
		sessionCode=sessionCode.substring(11);
//		String sessionCode = (String) session.getAttribute("code");
		logger.info("解析登录接口入参|用户名为:"+username+"|密码为:"+password+"|验证码为:" + code+"|sessionCode为:"+sessionCode);
		if (username.length() >= 129) {
			map.put("login", false);
			map.put("errorMassage", "用户名过长");
			return map;
		}
		if (password.length() >= 129) {
			map.put("login", false);
			map.put("errorMassage", "密码过长");
			return map;
		}
		if (!code.equalsIgnoreCase(sessionCode)) {
			map.put("login", false);
			map.put("errorMassage", "验证码错误");
			return map;
		}
		User user = userDao.getByUseInforName(username);

		if (null == user) {
			map.put("login", false);
			map.put("errorMassage", "用户名或密码错误");
			return map;
		} else
			try {
				if (!getMD5Str(password).equals(user.getPassWord())) {
					map.put("login", false);
					map.put("errorMassage", "用户名或密码错误");
					return map;
				}
			} catch (Exception e) {
				map.put("login", false);
				map.put("errorMassage", "用户名或密码错误");
				e.printStackTrace();
				return map;
			}
		map.put("login", true);
		map.put("errorMassage", "成功");
		//根据用户名username 查询角色权限
		HashSet<String> stringSet = new HashSet<>();
		stringSet = userDao.selectPermissions(username);
		List<String> list = new ArrayList<String>();
		for (String string : stringSet) {
			if(!"01".equals(string)&&!"02".equals(string)) {
				if(string.length()<2) {
					list.add(string);
				}else {
					//字符串，切割，存入set种
					String[] split = string.split(",");
					list = Arrays.asList(split);
				}
			}
		}
		map.put("setList", list);
		session.setAttribute("user", user);
		return map;
	}

	public String getMD5Str(String str) throws Exception {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			if (StringUtils.isBlank(str)) {
				return "";
			}
			md.update(str.getBytes());
			return new BigInteger(1, md.digest()).toString(16);
		} catch (Exception e) {
			throw new Exception("MD5加密出现错误，" + e.toString());
		}
	}
	
}
