package cn.cstm.air.service.impl;

import cn.cstm.air.domain.Userandrole;
import cn.cstm.air.mapper.UserandroleMapper;
import cn.cstm.air.service.IUserandroleService;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author author
 * @since 2020-04-14
 */
@Service
public class UserandroleServiceImpl extends ServiceImpl<UserandroleMapper, Userandrole> implements IUserandroleService {

}
