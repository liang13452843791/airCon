package cn.cstm.air.service.impl;

import cn.cstm.air.domain.Paramconfig;
import cn.cstm.air.mapper.ParamconfigMapper;
import cn.cstm.air.service.IParamconfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author author
 * @since 2020-04-26
 */
@Service
public class ParamconfigServiceImpl extends ServiceImpl<ParamconfigMapper, Paramconfig> implements IParamconfigService {

}
