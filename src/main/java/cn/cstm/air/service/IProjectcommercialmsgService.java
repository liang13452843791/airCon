package cn.cstm.air.service;

import cn.cstm.air.domain.Projectcommercialmsg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author author
 * @since 2020-04-17
 */
public interface IProjectcommercialmsgService extends IService<Projectcommercialmsg> {

}
