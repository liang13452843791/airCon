package cn.cstm.air.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;

import cn.cstm.air.domain.AgencyMsg;
import cn.cstm.air.exception.ServiceException;

public interface IUserService {

	JSONObject update(String password, String password1, String password2, String username);

	JSONObject insertAgencyMsg(String username, String agencyname, String agencyphone, String loginusername,
			String loginuserphone);

	void checkAgencyMsg(String username, String agencyname, String agencyphone, String loginusername,
			String loginuserphone) throws ServiceException;

	void checkUser(String password, String password1, String password2, String username) throws ServiceException;

	void checkUpdateAgencyMsg(String id, String agencyname, String agencyphone, String loginusername,
			String loginuserphone) throws ServiceException;

	JSONObject updateAgencyMsgById(String id, String agencyname, String agencyphone, String loginusername,
			String loginuserphone);

	void checkUpdateAgencyMsg(String agencyname) throws ServiceException;

	JSONObject selectListByAgencyName(String agencyname);

	JSONObject selectLikeByAgencyName(String agencyname);

	void check(String agencyname, String username) throws ServiceException;

	JSONObject selectAgencyListByAgencyNameAndUserName(String username, String agencyname);

	JSONObject selectAgencyListByUserName(String username);

	JSONObject selectAgencyList();

	void checkId(String id) throws ServiceException;

	JSONObject deleteAgencyMsgById(String id);

	JSONObject selectByAgencyName(String agencyname);

	List<AgencyMsg> selectAgencyNameByUserName(String header);

}
