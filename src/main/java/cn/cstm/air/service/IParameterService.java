package cn.cstm.air.service;

import cn.cstm.air.domain.Parameter;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author author
 * @since 2020-04-26
 */
public interface IParameterService extends IService<Parameter> {

}
