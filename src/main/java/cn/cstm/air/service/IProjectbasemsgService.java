package cn.cstm.air.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;

import cn.cstm.air.domain.Projectbasemsg;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author author
 * @since 2020-04-12
 */
public interface IProjectbasemsgService extends IService<Projectbasemsg> {

    public JSONObject audit(JSONObject jsonObject, HttpServletRequest req, HttpServletResponse resps);

    public JSONObject banchAudit(JSONObject jsonObject, HttpServletRequest req, HttpServletResponse resps);

//    List<LinkedHashMap<String, Object>> selectProjectCommsgList(String Reverse1, String ProjectType, String ProjectAddress, String AgencyName, String ProjectName, Date startDate, Date endDate, String regionalManager);

//    List<LinkedHashMap<String, Object>> selectProjectFammsgList(String Reverse1, String ProjectType, String ProjectAddress, String AgencyName, String CompoundName, Date startDate, Date endDate, String regionalManager);

    boolean hasAdminPower(String username);

	JSONObject deleteProjectById(String id);

	List<LinkedHashMap<String, Object>> selectProjectCommsgList(String Reverse1, String ProjectType, String ProjectAddress, String AgencyName, String ProjectName, Date startDate, Date endDate, List<String> agencyNames);

	List<LinkedHashMap<String, Object>> selectProjectFammsgList(String Reverse1, String ProjectType, String ProjectAddress, String AgencyName, String CompoundName, Date startDate, Date endDate,List<String> agencyNames);
}
