package cn.cstm.air.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;

public interface SysLoginUserService {

	
	public JSONObject loginAction(JSONObject jsonObject,HttpServletRequest req, HttpServletResponse resps);
}
