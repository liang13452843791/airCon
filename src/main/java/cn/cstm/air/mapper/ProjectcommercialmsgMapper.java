package cn.cstm.air.mapper;

import cn.cstm.air.domain.Projectcommercialmsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author author
 * @since 2020-04-17
 */
public interface ProjectcommercialmsgMapper extends BaseMapper<Projectcommercialmsg> {

}
