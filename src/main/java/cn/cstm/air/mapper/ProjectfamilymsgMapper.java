package cn.cstm.air.mapper;

import cn.cstm.air.domain.Projectfamilymsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author author
 * @since 2020-04-17
 */
public interface ProjectfamilymsgMapper extends BaseMapper<Projectfamilymsg> {

}
