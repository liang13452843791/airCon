package cn.cstm.air.mapper;

import cn.cstm.air.domain.Paramconfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author author
 * @since 2020-04-26
 */
public interface ParamconfigMapper extends BaseMapper<Paramconfig> {

}
