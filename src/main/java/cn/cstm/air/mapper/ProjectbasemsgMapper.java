package cn.cstm.air.mapper;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.cstm.air.domain.Projectbasemsg;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author author
 * @since 2020-04-12
 */
public interface ProjectbasemsgMapper extends BaseMapper<Projectbasemsg> {

    @Update("update projectbasemsg set reverse1= #{string} ,updatetime=#{updatetime} WHERE reverse1 = #{string2} and LoginDate between #{startDate} and #{endDate}")
    void banchUpdate(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("string") String string, @Param("string2") String string2, @Param("updatetime") Date updatetime);

/*
    @Select("<script>" +
            "SELECT *,com.Reverse1 AS Reverse1_commercial " +
            "FROM projectbasemsg base, projectcommercialmsg com " +
            "WHERE " +
            "base.ProjectNumber = com.ProjectNumber " +
            "<if test='Reverse1!=null and Reverse1 != \"\" '>" +
            "AND base.Reverse1 = #{Reverse1} " +
            "</if>" +
            "<if test='ProjectType!=null and ProjectType != \"\" '>" +
            "AND base.ProjectType =#{ProjectType} " +
            "</if>" +
            "<if test='ProjectAddress!=null and ProjectAddress != \"\" '>" +
            " AND base.ProjectAddress LIKE CONCAT('%',#{ProjectAddress},'%') " +
            "</if>" +
            "<if test='AgencyName!=null and AgencyName != \"\" '>" +
            " AND base.AgencyName LIKE CONCAT('%',#{AgencyName},'%') " +
            "</if>" +
            "<if test='ProjectName!=null and ProjectName != \"\" '>" +
            " AND base.ProjectName LIKE CONCAT('%',#{ProjectName},'%') " +
            "</if>" +
            "<if test='startDate!=null '>" +
            " AND base.LoginDate between #{startDate} and #{endDate}" +
            "</if>" +
            "<if test='regionalManager!=null and regionalManager != \"\" '>" +
            " AND base.RegionalManager =#{regionalManager}" +
            "</if>" +
            " AND base.States = 1" +
            " order by base.CreateTime desc" +
            "</script>")
    List<LinkedHashMap<String, Object>> selectProjectCommsgList(@Param("Reverse1") String Reverse1, @Param("ProjectType") String ProjectType, @Param("ProjectAddress") String ProjectAddress, @Param("AgencyName") String AgencyName, @Param("ProjectName") String ProjectName, @Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("regionalManager") String regionalManager);

    @Select("<script>" +
            "SELECT * ,fam.Reverse1 AS Reverse1_family " +
            "FROM projectbasemsg base, projectfamilymsg fam " +
            "WHERE " +
            "base.ProjectNumber = fam.ProjectNumber " +
            "<if test='Reverse1!=null and Reverse1 != \"\" '>" +
            "AND base.Reverse1 = #{Reverse1} " +
            "</if>" +
            "<if test='ProjectType!=null and ProjectType != \"\" '>" +
            "AND base.ProjectType =#{ProjectType} " +
            "</if>" +
            "<if test='ProjectAddress!=null and ProjectAddress != \"\" '>" +
            " AND base.ProjectAddress LIKE CONCAT('%',#{ProjectAddress},'%') " +
            "</if>" +
            "<if test='AgencyName!=null and AgencyName != \"\" '>" +
            " AND base.AgencyName LIKE CONCAT('%',#{AgencyName},'%') " +
            "</if>" +
            "<if test='CompoundName!=null and CompoundName != \"\" '>" +
            " AND base.CompoundName LIKE CONCAT('%',#{CompoundName},'%') " +
            "</if>" +
            "<if test='startDate!=null'>" +
            " AND base.LoginDate between #{startDate} and #{endDate}" +
            "</if>" +
            "<if test='regionalManager!=null and regionalManager != \"\" '>" +
            " AND base.RegionalManager =#{regionalManager}" +
            "</if>" +
            " AND base.States = 1" +
            " order by base.CreateTime desc" +
            "</script>")
    List<LinkedHashMap<String, Object>> selectProjectFammsgList(@Param("Reverse1") String Reverse1, @Param("ProjectType") String ProjectType, @Param("ProjectAddress") String ProjectAddress, @Param("AgencyName") String AgencyName, @Param("CompoundName") String CompoundName, @Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("regionalManager") String regionalManager);
*/

    @Update("UPDATE projectbasemsg SET States = '0'  WHERE id = #{id}")
	Integer deleteProjectById(String id);
    
    @Select("<script>" +
            "SELECT *,pbm.Reverse3 AS Reverse3_pbm ,pcm.Reverse1 AS Reverse1_commercial " +
            "FROM projectbasemsg pbm left join projectcommercialmsg pcm on pcm.ProjectNumber = pbm.ProjectNumber " +
            "WHERE pbm.States = 1" +
            "<if test='Reverse1!=null and Reverse1 != \"\" '>" +
            "AND pbm.Reverse1 = #{Reverse1} " +
            "</if>" +
            "<if test='ProjectType!=null and ProjectType != \"\" '>" +
            "AND pbm.ProjectType = #{ProjectType} " +
            "</if>" +
            "<if test='ProjectAddress!=null and ProjectAddress != \"\" '>" +
            " AND pbm.ProjectAddress LIKE CONCAT('%',#{ProjectAddress},'%') " +
            "</if>" +
            "<if test='agencyNameList!=null and agencyNameList.size()>0'>" +
            "and"+
            "<foreach collection=\"agencyNameList\" index=\"index\" item=\"item\"\r\n" + 
            "			open=\"(\" separator=\"OR\" close=\")\">\r\n" + 
            "			 AgencyName LIKE '%${item}%'\r\n" + 
            "		</foreach>"+
            "</if>" +
            "<if test='ProjectName!=null and ProjectName != \"\" '>" +
            " AND pcm.ProjectName LIKE CONCAT('%',#{ProjectName},'%') " +
            "</if>" +
            "<if test='startDate!=null '>" +
            " AND pbm.LoginDate between #{startDate} and #{endDate}" +
            "</if>" +
            " order by pbm.LoginDate desc" +
            "</script>")
    List<LinkedHashMap<String, Object>> selectProjectCommsgList(@Param("Reverse1") String Reverse1, @Param("ProjectType") String ProjectType, @Param("ProjectAddress") String ProjectAddress, @Param("AgencyName") String AgencyName, @Param("ProjectName") String ProjectName, @Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("agencyNameList") List<String> agencyNameList);
    
    @Select("<script>" +
            "SELECT * ,pbm.Reverse3 AS Reverse3_pbm ,pfm.Reverse1 AS Reverse1_family,pfm.Reverse2 AS Reverse2_family " +
            "FROM projectbasemsg pbm left join projectfamilymsg pfm on pfm.ProjectNumber = pbm.ProjectNumber " +
            "WHERE pbm.States = 1" +
            "<if test='Reverse1!=null and Reverse1 != \"\" '>" +
            "AND pbm.Reverse1 = #{Reverse1} " +
            "</if>" +
            "<if test='ProjectType!=null and ProjectType != \"\" '>" +
            "AND pbm.ProjectType = #{ProjectType} " +
            "</if>" +
            "<if test='ProjectAddress!=null and ProjectAddress != \"\" '>" +
            " AND pbm.ProjectAddress LIKE CONCAT('%',#{ProjectAddress},'%') " +
            "</if>" +
			"<if test='agencyNameList!=null and agencyNameList.size()>0'>" +
            "and"+
			"<foreach collection=\"agencyNameList\" index=\"index\" item=\"item\"\r\n" + 
			"			open=\"(\" separator=\"OR\" close=\")\">\r\n" + 
			"			 AgencyName LIKE '%${item}%'\r\n" + 
			"		</foreach>"+
			"</if>" +
            "<if test='CompoundName!=null and CompoundName != \"\" '>" +
            " AND pfm.CompoundName LIKE CONCAT('%',#{CompoundName},'%') " +
            "</if>" +
            "<if test='startDate!=null'>" +
            " AND pbm.LoginDate between #{startDate} and #{endDate}" +
            "</if>" +
//            "<if test='regionalManager!=null and regionalManager != \"\" '>" +
//            " AND pbm.RegionalManager =#{regionalManager}" +
//            "</if>" +
            " order by pbm.LoginDate desc" +
            "</script>")
    List<LinkedHashMap<String, Object>> selectProjectFammsgList(@Param("Reverse1") String Reverse1, @Param("ProjectType") String ProjectType, @Param("ProjectAddress") String ProjectAddress, @Param("AgencyName") String AgencyName, @Param("CompoundName") String CompoundName, @Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("agencyNameList") List<String> agencyNameList);
}
