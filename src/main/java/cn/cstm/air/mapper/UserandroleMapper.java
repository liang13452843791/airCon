package cn.cstm.air.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.cstm.air.domain.Userandrole;
import io.lettuce.core.dynamic.annotation.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author author
 * @since 2020-04-14
 */
public interface UserandroleMapper extends BaseMapper<Userandrole> {
@Select("select * from userandrole where username=#{username}")
	List<Userandrole> getByUsername(@Param("username")String userName);
}
