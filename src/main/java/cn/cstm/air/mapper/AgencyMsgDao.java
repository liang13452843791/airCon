package cn.cstm.air.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.cstm.air.domain.AgencyMsg;

public interface AgencyMsgDao extends BaseMapper<AgencyMsg>{

	@Insert("insert into agencymsg (id, AgencyName, AgencyPhone,LoginUserName, LoginUserPhone, States, CreateTime, Username) "
			+ "values (#{id},#{AgencyName},#{AgencyPhone},#{LoginUserName},#{LoginUserPhone},"
			+ "#{States},#{CreateTime},#{UserName})")
	Integer insertSelective(AgencyMsg agencyMsg);
	
	@Update("UPDATE agencymsg SET AgencyPhone=#{AgencyPhone},LoginUserName=#{LoginUserName},LoginUserPhone=#{LoginUserPhone},"
			+ "UpdateTime=#{UpdateTime} WHERE AgencyName = #{AgencyName} and id = #{id}")
	Integer updateByPrimaryKeySelective(AgencyMsg agencyMsg);

	@Select("select * from agencymsg WHERE AgencyName LIKE '%${AgencyName}%'")
	List<AgencyMsg> selectLikeByAgencyName(@Param("AgencyName")String AgencyName);

	@Select("select * from agencymsg WHERE AgencyName = #{AgencyName} and States = 1")
	List<AgencyMsg> selectListByAgencyName(String agencyName);

	@Select("select * from agencymsg WHERE AgencyName LIKE '%${AgencyName}%' and UserName = #{UserName} and States = '1' order by CreateTime desc")
	List<AgencyMsg> selectAgencyListByAgencyNameAndUserName(@Param("UserName")String UserName, @Param("AgencyName")String AgencyName);

	@Select("select * from agencymsg WHERE UserName = #{UserName} and States = '1'")
	List<AgencyMsg> selectAgencyListByUserName(@Param("UserName")String UserName);

	@Select("select * from agencymsg WHERE States = '1'")
	List<AgencyMsg> selectAgencyList();

	@Update("UPDATE agencymsg SET States = '0'  WHERE id = #{id}")
	Integer deleteAgencyMsgById(@Param("id")String id);

	@Select("select * from agencymsg WHERE AgencyName = #{agencyName} and States = 1")
	AgencyMsg selectByAgencyName(@Param("agencyName")String agencyName);

	@Select("select AgencyName from agencymsg WHERE States = '1'" + 
            "AND UserName = #{username} ")
	List<AgencyMsg> selectAgencyNameByUserName(@Param("username")String username);
}
