package cn.cstm.air.mapper;

import java.util.Date;
import java.util.HashSet;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.cstm.air.domain.User;

public interface UserDao extends BaseMapper<User> {

	@Select("SELECT password as password FROM user WHERE UserName = #{username} and Status = 1")
	User getByUserName(@Param("username")String username);
	
	@Update("update user set PassWord= #{PassWord} WHERE UserName = #{UserName}")
	int updatePassword(User newUser);

    @Update("UPDATE user SET PassWord=#{PassWord} WHERE UserName = #{UserName}")
	int updatePassword(@Param("UserName")String UserName, @Param("PassWord")String PassWord);
   
    @Update("UPDATE user SET PassWord=#{PassWord},updateTime=#{date} WHERE UserName = #{UserName}")
	Integer updatePassword(@Param("UserName")String UserName, @Param("PassWord")String PassWord, @Param("date")Date date);
    
    @Select("SELECT *  FROM user WHERE UserName = #{username} and Status = 1")
	User getByUseInforName(@Param("username")String username);

	@Select("select authorityCode from roleandauthority  where roleName =(select distinct roleName from userandrole where UserName = #{username} and Status = 1)")
	HashSet<String> selectPermissions(@Param("username")String username);
}
