package cn.cstm.air.controller;

import cn.cstm.air.domain.Paramconfig;
import cn.cstm.air.service.IParamconfigService;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Effect
 * @Author Zll
 * @Date 2020-4-26 18:12:24
 */
@RestController
public class SelectController {
    private Log log = LogFactory.getLog(getClass());
    private final IParamconfigService paramconfigService;

    @Autowired
    public SelectController(IParamconfigService paramconfigService) {
        this.paramconfigService = paramconfigService;
    }

    /**
     * 审核状态
     */
    @PostMapping("/selectReverse1")
    @Transactional(rollbackFor = {Exception.class})
    public JSONObject selectReverse1() {
        JSONObject returnMsg = new JSONObject();
        log.info("查询审核状态:------begin------");
        List<Map<String, Object>> maps = paramconfigService.listMaps(
                new QueryWrapper<Paramconfig>().select("ParamKey AS code", "ParamValue AS value").allEq(ImmutableMap.of("ParamType", "审核状态 ")));
        log.info("查询结果:" + maps);
        returnMsg.put("listInfo", maps);
        log.info("查询审核状态:------end------");
        return returnMsg;
    }

    /**
     * 项目类型
     */
    @PostMapping("/selectProjectType")
    @Transactional(rollbackFor = {Exception.class})
    public JSONObject selectProjectType() {
        JSONObject returnMsg = new JSONObject();
        log.info("查询项目类型:------begin------");
        List<Map<String, Object>> maps = paramconfigService.listMaps(new QueryWrapper<Paramconfig>().select("ParamKey AS code", "ParamValue AS value").eq("ParamType", "项目类型"));
        log.info("查询结果:" + maps);
        returnMsg.put("listInfo", maps);
        log.info("查询项目类型:------end------");
        return returnMsg;
    }

    /**
     * 项目来源
     */
    @PostMapping("/selectProjectOrigin")
    @Transactional(rollbackFor = {Exception.class})
    public JSONObject selectProjectOrigin() {
        JSONObject returnMsg = new JSONObject();
        log.info("查询项目来源:------begin------");
        List<Map<String, Object>> maps = paramconfigService.listMaps(new QueryWrapper<Paramconfig>().select("ParamKey AS code", "ParamValue AS value").eq("ParamType", "项目来源"));
        log.info("查询结果:" + maps);
        returnMsg.put("listInfo", maps);
        log.info("查询项目来源:------end------");
        return returnMsg;
    }

    /**
     * 预选机型
     */
    @PostMapping("/selectPreSelectedModel")
    @Transactional(rollbackFor = {Exception.class})
    public JSONObject selectPreSelectedModel() {
        JSONObject returnMsg = new JSONObject();
        log.info("查询预选机型:------begin------");
        List<Map<String, Object>> maps = paramconfigService.listMaps(new QueryWrapper<Paramconfig>().select("ParamKey AS code", "ParamValue AS value").eq("ParamType", "预选机型"));
        log.info("查询结果:" + maps);
        returnMsg.put("listInfo", maps);
        log.info("查询预选机型:------end------");
        return returnMsg;
    }

    /**
     * 是否跨区
     */
    @PostMapping("/selectIfCrossArea")
    @Transactional(rollbackFor = {Exception.class})
    public JSONObject selectIfCrossArea() {
        JSONObject returnMsg = new JSONObject();
        log.info("查询是否跨区:------begin------");
        List<Map<String, Object>> maps = paramconfigService.listMaps(new QueryWrapper<Paramconfig>().select("ParamKey AS code", "ParamValue AS value").eq("ParamType", "是否跨区"));
        log.info("查询结果:" + maps);
        returnMsg.put("listInfo", maps);
        log.info("查询是否跨区:------end------");
        return returnMsg;
    }

    /**
     * 成功标识
     */
    @PostMapping("/selectReverse3")
    @Transactional(rollbackFor = {Exception.class})
    public JSONObject selectReverse3() {
        JSONObject returnMsg = new JSONObject();
        log.info("查询成功标识:------begin------");
        List<Map<String, Object>> maps = paramconfigService.listMaps(new QueryWrapper<Paramconfig>().select("ParamKey AS code", "ParamValue AS value").eq("ParamType", "成功标识"));
        log.info("查询结果:" + maps);
        returnMsg.put("listInfo", maps);
        log.info("查询成功标识:------end------");
        return returnMsg;
    }

    
    /**
     * 成功率
     */
    @PostMapping("/selectSuccessRate")
    @Transactional(rollbackFor = {Exception.class})
    public JSONObject selectSuccessRate() {
        JSONObject returnMsg = new JSONObject();
        log.info("查询成功率:------begin------");
        List<Map<String, Object>> maps = paramconfigService.listMaps(new QueryWrapper<Paramconfig>().select("ParamKey AS code", "ParamValue AS value").eq("ParamType", "成功率"));
        log.info("查询结果:" + maps);
        returnMsg.put("listInfo", maps);
        log.info("查询成功率:------end------");
        return returnMsg;
    }

    /**
     * 户型
     */
    @PostMapping("/selectHouseType")
    @Transactional(rollbackFor = {Exception.class})
    public JSONObject selectHouseType() {
        JSONObject returnMsg = new JSONObject();
        log.info("查询户型:------begin------");
        List<Map<String, Object>> maps = paramconfigService.listMaps(new QueryWrapper<Paramconfig>().select("ParamKey AS code", "ParamValue AS value").eq("ParamType", "户型"));
        log.info("查询结果:" + maps);
        returnMsg.put("listInfo", maps);
        log.info("查询户型:------end------");
        return returnMsg;
    }

    /**
     * 投标形式
     */
    @PostMapping("/selectTenderForm")
    @Transactional(rollbackFor = {Exception.class})
    public JSONObject selectTenderForm() {
        JSONObject returnMsg = new JSONObject();
        log.info("查询招标形式:------begin------");
        List<Map<String, Object>> maps = paramconfigService.listMaps(new QueryWrapper<Paramconfig>().select("ParamKey AS code", "ParamValue AS value").eq("ParamType", "招标形式"));
        log.info("查询结果:" + maps);
        returnMsg.put("listInfo", maps);
        log.info("查询招标形式:------end------");
        return returnMsg;
    }

    /**
     * 工程进度
     */
    @PostMapping("/selectProjectSchedule")
    @Transactional(rollbackFor = {Exception.class})
    public JSONObject selectProjectSchedule() {
        JSONObject returnMsg = new JSONObject();
        log.info("查询工程进度:------begin------");
        List<Map<String, Object>> maps = paramconfigService.listMaps(new QueryWrapper<Paramconfig>().select("ParamKey AS code", "ParamValue AS value").eq("ParamType", "工程进度"));
        log.info("查询结果:" + maps);
        returnMsg.put("listInfo", maps);
        log.info("查询工程进度:------end------");
        return returnMsg;
    }
}
