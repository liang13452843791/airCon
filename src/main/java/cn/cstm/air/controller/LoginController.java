package cn.cstm.air.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import cn.cstm.air.service.SysLoginUserService;
import cn.cstm.air.util.CodeUtil;

@RestController
public class LoginController {

	
	@Autowired 
	private SysLoginUserService loginUserService ;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PostMapping(value = "/getCode")
	public JSONObject getCode(HttpServletRequest req, HttpServletResponse resp) {
		JSONObject codeMap = CodeUtil.generateCodeAndPic();
		logger.info("二维码生成："+codeMap.toJSONString());
		// 将四位数字的验证码保存到Session中。
		HttpSession session = req.getSession();
		session.setAttribute("code", codeMap.get("code").toString());
//		codeMap.put("sessionid", session.getId());
		codeMap.put("sessionid", session.getAttribute("code").toString());
		// 禁止图像缓存。
		resp.setHeader("Pragma", "no-cache");
		resp.setHeader("Cache-Control", "no-cache");
		resp.setDateHeader("Expires", -1);
		resp.setContentType("image/jpeg");
		// 将图像输出到Servlet输出流中。
		logger.info("放进去了吗?code="+session.getAttribute("code").toString()+"|session.getId():"+session.getId());
		return codeMap;
	}
	
	@PostMapping(value = "/loginAction")
	public JSONObject login_in(@RequestBody JSONObject jsonObject,HttpServletRequest req, HttpServletResponse resp) {
		logger.info("登录接口loginAction-req.getHeader(\"Cookie\")："+req.getHeader("Cookie"));
//		HttpSession session = req.getSession();
//		String id = session.getId();
//		logger.info("登录接口loginAction-session.getId()："+id);
		JSONObject object=loginUserService.loginAction(jsonObject, req, resp);
		getCode(req,resp);
		return object;
	}
	
	@GetMapping(value = "/notLogin")
	public JSONObject login_in() {
		JSONObject object=new JSONObject();
		object.put("login",false);
		object.put("errorMassage","用户未登录");
		return object;
	}
	
	@PostMapping(value = "/loginOut")
	public JSONObject login_out(HttpServletRequest req, HttpServletResponse resp) {
		HttpSession session = req.getSession();
		session.removeAttribute("user");
		JSONObject object=new JSONObject();
		object.put("loginOut",true);
		object.put("errorMassage","退出成功");
		return object;
	}
}
