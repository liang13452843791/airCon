package cn.cstm.air.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.ImmutableMap;

import cn.cstm.air.exception.ServiceException;
import cn.cstm.air.service.IUserService;
import cn.cstm.air.util.ValueOfMD5;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "账户管理")
@RestController
public class UserController {

	@Autowired
	public IUserService userService;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());


	@PostMapping(value = "updatePassword")
	@ResponseBody
	@ApiOperation(value="修改密码接口",notes="原密码匹配一致的情况下，根据用户名修改为新密码")
	public JSONObject updatePassword(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		JSONObject json = new JSONObject();
		try {
			String pwd = jsonObject.getString("password");
			if(StringUtils.isBlank(pwd)) {
				throw new ServiceException("99999999", "原密码不能为空");
			}
			String password = ValueOfMD5.string2MD5(pwd);
			String password1 = jsonObject.getString("password1");
			logger.info("用户输入新密码第一次：" + password1);
			String password2 = jsonObject.getString("password2");
			logger.info("用户输入新密码第二次：" + password2);
//			String username = jsonObject.getString("username");
			logger.info("当前用户的用户名：" + request.getHeader("user"));
			userService.checkUser(password,password1,password2,request.getHeader("user"));
			json = userService.update(password,password1,password2,request.getHeader("user"));
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ServiceException) {
				json.put("RespInfo", ImmutableMap.of("errorCode", ((ServiceException) e).getErrCode(), "errorMessage", ((ServiceException) e).getErrMsg()));
			}else{
				json.put("RespInfo", ImmutableMap.of("errorCode", "99999999", "errorMessage", "数据操作失败"));
			}
		}
		return json;
	}
	
	@PostMapping(value = "insertAgencyMsgSelective")
	@ResponseBody
	@ApiOperation(value="添加经销商信息接口")
	public JSONObject insertAgencyMsgSelective(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		JSONObject json = new JSONObject();
		try {
//			String username = jsonObject.getString("username");
			String agencyname = jsonObject.getString("agencyname");
			String agencyphone = jsonObject.getString("agencyphone");
			String loginusername = jsonObject.getString("loginusername");
			String loginuserphone = jsonObject.getString("loginuserphone");
			userService.checkAgencyMsg(request.getHeader("user"),agencyname,agencyphone,loginusername,loginuserphone);
			json = userService.insertAgencyMsg(request.getHeader("user"),agencyname,agencyphone,loginusername,loginuserphone);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ServiceException) {
				json.put("RespInfo", ImmutableMap.of("errorCode", ((ServiceException) e).getErrCode(), "errorMessage", ((ServiceException) e).getErrMsg()));
			}else{
				json.put("RespInfo", ImmutableMap.of("errorCode", "99999999", "errorMessage", "数据操作失败"));
			}
		}
		return json;
	}
	
	@PostMapping(value = "updateAgencyMsgById")
	@ResponseBody
	@ApiOperation(value="修改经销商信息接口")
	public JSONObject updateAgencyMsgById(@RequestBody JSONObject jsonObject){
		JSONObject json = new JSONObject();
		try {
			String id = jsonObject.getString("id");
			String agencyname = jsonObject.getString("agencyname");
			String agencyphone = jsonObject.getString("agencyphone");
			String loginusername = jsonObject.getString("loginusername");
			String loginuserphone = jsonObject.getString("loginuserphone");
			userService.checkUpdateAgencyMsg(id,agencyname,agencyphone,loginusername,loginuserphone);
			json = userService.updateAgencyMsgById(id,agencyname,agencyphone,loginusername,loginuserphone);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ServiceException) {
				json.put("RespInfo", ImmutableMap.of("errorCode", ((ServiceException) e).getErrCode(), "errorMessage", ((ServiceException) e).getErrMsg()));
			}else{
				json.put("RespInfo", ImmutableMap.of("errorCode", "99999999", "errorMessage", "数据操作失败"));
			}
		}
		return json;
	}
	
	@PostMapping(value = "selectLikeByAgencyName")
	@ResponseBody
	@ApiOperation(value="根据经销商名称模糊查询经销商信息集合")
	public JSONObject selectLikeByAgencyName(@RequestBody JSONObject jsonObject){
		logger.info("根据经销商名称模糊查询经销商信息集合selectLikeByAgencyName接口入参|" + jsonObject);
		JSONObject json = new JSONObject();
		try {
			String agencyname = jsonObject.getString("agencyname");
			userService.checkUpdateAgencyMsg(agencyname);
			json = userService.selectLikeByAgencyName(agencyname);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ServiceException) {
				json.put("RespInfo", ImmutableMap.of("errorCode", ((ServiceException) e).getErrCode(), "errorMessage", ((ServiceException) e).getErrMsg()));
			}else{
				json.put("RespInfo", ImmutableMap.of("errorCode", "99999999", "errorMessage", "数据操作失败"));
			}
		}
		return json;
	}
	
	@PostMapping(value = "selectByAgencyName")
	@ResponseBody
	@ApiOperation(value="根据经销商名称查询经销商信息")
	public JSONObject selectByAgencyName(@RequestBody JSONObject jsonObject){
		logger.info("根据经销商名称查询经销商信息selectByAgencyName接口入参|" + jsonObject);
		JSONObject json = new JSONObject();
		try {
			String agencyname = jsonObject.getString("agencyname");
			userService.checkUpdateAgencyMsg(agencyname);
			json = userService.selectByAgencyName(agencyname);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ServiceException) {
				json.put("RespInfo", ImmutableMap.of("errorCode", ((ServiceException) e).getErrCode(), "errorMessage", ((ServiceException) e).getErrMsg()));
			}else{
				json.put("RespInfo", ImmutableMap.of("errorCode", "99999999", "errorMessage", "数据操作失败"));
			}
		}
		return json;
	}
	
	@PostMapping(value = "selectListByAgencyName")
	@ResponseBody
	@ApiOperation(value="根据经销商名称查询经销商信息集合")
	public JSONObject selectListByAgencyName(@RequestBody JSONObject jsonObject){
		logger.info("根据经销商名称查询经销商信息集合selectListByAgencyName接口入参|" + jsonObject);
		JSONObject json = new JSONObject();
		try {
			String agencyname = jsonObject.getString("agencyname");
			userService.checkUpdateAgencyMsg(agencyname);
			json = userService.selectListByAgencyName(agencyname);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ServiceException) {
				json.put("RespInfo", ImmutableMap.of("errorCode", ((ServiceException) e).getErrCode(), "errorMessage", ((ServiceException) e).getErrMsg()));
			}else{
				json.put("RespInfo", ImmutableMap.of("errorCode", "99999999", "errorMessage", "数据操作失败"));
			}
		}
		return json;
	}
	
	@PostMapping(value = "selectAgencyListByAgencyNameAndUserName")
	@ResponseBody
	@ApiOperation(value="根据用户名和经销商名称查询经销商信息集合")
	public JSONObject selectAgencyListByAgencyNameAndUserName(@RequestBody JSONObject jsonObject, HttpServletRequest request){
		logger.info("根据用户名和经销商名称查询经销商信息集合selectAgencyListByAgencyNameAndUserName接口入参|" + jsonObject);
		JSONObject json = new JSONObject();
		try {
			String agencyname = jsonObject.getString("agencyname");
//			String username = jsonObject.getString("username");
			
			userService.check(agencyname,request.getHeader("user"));
			json = userService.selectAgencyListByAgencyNameAndUserName(request.getHeader("user"),agencyname);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ServiceException) {
				json.put("RespInfo", ImmutableMap.of("errorCode", ((ServiceException) e).getErrCode(), "errorMessage", ((ServiceException) e).getErrMsg()));
			}else{
				json.put("RespInfo", ImmutableMap.of("errorCode", "99999999", "errorMessage", "数据操作失败"));
			}
		}
		return json;
	}
	
	@PostMapping(value = "selectAgencyListByUserName")
	@ResponseBody
	@ApiOperation(value="根据用户名查询经销商信息集合")
	public JSONObject selectAgencyListByUserName(HttpServletRequest request){
//		logger.info("根据用户名查询经销商信息集合selectAgencyListByUserName接口入参|前端写死admin，下版本改|" + jsonObject);
		logger.info("根据用户名查询经销商信息集合selectAgencyListByUserName-用户信息：" + request.getHeader("user"));
		JSONObject json = new JSONObject();
		try {
			userService.checkUpdateAgencyMsg(request.getHeader("user"));
			json = userService.selectAgencyListByUserName(request.getHeader("user"));
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ServiceException) {
				json.put("RespInfo", ImmutableMap.of("errorCode", ((ServiceException) e).getErrCode(), "errorMessage", ((ServiceException) e).getErrMsg()));
			}else{
				json.put("RespInfo", ImmutableMap.of("errorCode", "99999999", "errorMessage", "数据操作失败"));
			}
		}
		return json;
	}
	
	@PostMapping(value = "selectAgencyList")
	@ResponseBody
	@ApiOperation(value="查询所有经销商信息")
	public JSONObject selectAgencyList(HttpServletRequest request){
		JSONObject json = new JSONObject();
		try {
			json = userService.selectAgencyList();
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ServiceException) {
				json.put("RespInfo", ImmutableMap.of("errorCode", ((ServiceException) e).getErrCode(), "errorMessage", ((ServiceException) e).getErrMsg()));
			}else{
				json.put("RespInfo", ImmutableMap.of("errorCode", "99999999", "errorMessage", "数据操作失败"));
			}
		}
		return json;
	}
	
	@PostMapping(value = "selectAgencyList1")
	@ResponseBody
	@ApiOperation(value="查询所有经销商信息")
	public JSONObject selectAgencyList1(HttpServletRequest request){
		JSONObject json = new JSONObject();
		try {
			if("admin".equals(request.getHeader("user"))) {
				json = userService.selectAgencyList();
			}else {
				json = userService.selectAgencyListByUserName(request.getHeader("user"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ServiceException) {
				json.put("RespInfo", ImmutableMap.of("errorCode", ((ServiceException) e).getErrCode(), "errorMessage", ((ServiceException) e).getErrMsg()));
			}else{
				json.put("RespInfo", ImmutableMap.of("errorCode", "99999999", "errorMessage", "数据操作失败"));
			}
		}
		return json;
	}
	
	@PostMapping(value = "deleteAgencyMsgById")
	@ResponseBody
	@ApiOperation(value="根据ID删除经销商数据")
	public JSONObject deleteAgencyMsgById(@RequestBody JSONObject jsonObject){
		JSONObject json = new JSONObject();
		try {
			String id = jsonObject.getString("id");
			userService.checkId(id);
			json = userService.deleteAgencyMsgById(id);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof ServiceException) {
				json.put("RespInfo", ImmutableMap.of("errorCode", ((ServiceException) e).getErrCode(), "errorMessage", ((ServiceException) e).getErrMsg()));
			}else{
				json.put("RespInfo", ImmutableMap.of("errorCode", "99999999", "errorMessage", "数据操作失败"));
			}
		}
		return json;
	}
}
