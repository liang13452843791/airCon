package cn.cstm.air.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

import cn.cstm.air.domain.AgencyMsg;
import cn.cstm.air.domain.Parameter;
import cn.cstm.air.domain.Projectbasemsg;
import cn.cstm.air.domain.Projectcommercialmsg;
import cn.cstm.air.domain.Projectfamilymsg;
import cn.cstm.air.exception.ServiceException;
import cn.cstm.air.service.IParameterService;
import cn.cstm.air.service.IProjectbasemsgService;
import cn.cstm.air.service.IProjectcommercialmsgService;
import cn.cstm.air.service.IProjectfamilymsgService;
import cn.cstm.air.service.IUserService;
import cn.cstm.air.util.PageInfo;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author author
 * @since 2020-04-12
 */
@RestController
public class ProjectbasemsgController {
    private Log log = LogFactory.getLog(getClass());
    private final IProjectbasemsgService projectbasemsgService;
    private final IProjectcommercialmsgService projectcommercialmsgService;
    private final IProjectfamilymsgService projectfamilymsgService;
    private final IParameterService parameterService;
    private final IUserService userService;

    @Autowired
    public ProjectbasemsgController(IProjectbasemsgService projectbasemsgService, IProjectcommercialmsgService projectcommercialmsgService, IProjectfamilymsgService projectfamilymsgService, IParameterService parameterService, IUserService userService) {
        this.projectbasemsgService = projectbasemsgService;
        this.projectcommercialmsgService = projectcommercialmsgService;
        this.projectfamilymsgService = projectfamilymsgService;
        this.parameterService = parameterService;
        this.userService = userService;
    }

    @PostMapping("/aduit")
    public JSONObject audit(@RequestBody JSONObject jsonObject, HttpServletRequest req, HttpServletResponse resps) {
        return projectbasemsgService.audit(jsonObject, req, resps);
    }

    @PostMapping("/banchAudit")
    public JSONObject banchAudit(@RequestBody JSONObject jsonObject, HttpServletRequest req, HttpServletResponse resps) {
        return projectbasemsgService.banchAudit(jsonObject, req, resps);
    }


    /**
     * 获取接收到的数据
     */
    @PostMapping("/addOrUpdateProjectmsg")
    @Transactional(rollbackFor = {Exception.class})
    @SuppressWarnings("unchecked")
    public JSONObject addProjectmsg(@RequestBody JSONObject jsonObject, HttpServletRequest request) {
        log.info("项目信息管理新增或修改接口addOrUpdateProjectmsg入参|" + jsonObject);
        JSONObject returnMsg = new JSONObject();
        String state, message;
        Map<String, Object> projectInfo = jsonObject.getObject("projectInfo", Map.class);
        try {
            log.info("项目信息管理新增或修改接口addOrUpdateProjectmsg-用户信息：" + request.getHeader("user"));
            Projectbasemsg projectbasemsg = jsonObject.getObject("projectInfo", Projectbasemsg.class);
            projectbasemsg.setStates("1");
            if ("家用".equals(projectbasemsg.getProjectType()))
                projectbasemsg.setProjectType("pt02");
            else if ("商用".equals(projectbasemsg.getProjectType()))
                projectbasemsg.setProjectType("pt01");
            if ("是".equals(projectbasemsg.getIfCrossArea()))
                projectbasemsg.setIfCrossArea("01");
            else if ("否".equals(projectbasemsg.getIfCrossArea()))
                projectbasemsg.setIfCrossArea("02");
            if ("成功".equals(projectbasemsg.getReverse3()))
                projectbasemsg.setProjectType("cg1");
            else if ("失败".equals(projectbasemsg.getReverse3()))
                projectbasemsg.setProjectType("cg2");
            //psm01:商用多联 psm02:模块系列 psm03:户型机系列 psm04:风管机系列 psm05:终端设备系列 psm06:热水机系列 psm07:其他
            StringBuilder preSelectedModel = new StringBuilder();
            List<String> preSelectedModelList = (List<String>) projectInfo.getOrDefault("PreSelectedModel", new ArrayList<>());
            preSelectedModelList.forEach(machine -> {
                switch (machine) {
                    case "商用多联":
                        preSelectedModel.append("psm01,");
                        break;
                    case "模块系列":
                        preSelectedModel.append("psm02,");
                        break;
                    case "户式水机系列":
                        preSelectedModel.append("psm03,");
                        break;
                    case "风管机系列":
                        preSelectedModel.append("psm04,");
                        break;
                    case "终端设备系列":
                        preSelectedModel.append("psm05,");
                        break;
                    case "热水机系列":
                        preSelectedModel.append("psm06,");
                        break;
                    case "其他":
                        preSelectedModel.append("psm07,");
                        break;
                    case "家用多联":
                        preSelectedModel.append("psm08,");
                        break;
                    default:
                        break;
                }
            });
            if (preSelectedModel.length() != 0)
                projectbasemsg.setPreSelectedModel(preSelectedModel.substring(0, preSelectedModel.length() - 1));
            //sr01:30% sr02:50% sr03:80% sr04:95% sr05:100%
            switch (!Strings.isNullOrEmpty(projectbasemsg.getSuccessRate()) ? projectbasemsg.getSuccessRate() : "") {
                case "30%":
                    projectbasemsg.setSuccessRate("sr01");
                    break;
                case "50%":
                    projectbasemsg.setSuccessRate("sr02");
                    break;
                case "80%":
                    projectbasemsg.setSuccessRate("sr03");
                    break;
                case "95%":
                    projectbasemsg.setSuccessRate("sr04");
                    break;
                case "100%":
                    projectbasemsg.setSuccessRate("sr05");
                    break;
                default:
                    break;
            }
            int count = projectbasemsgService.count(new QueryWrapper<Projectbasemsg>().eq("ProjectNumber", projectbasemsg.getProjectNumber()));
            if (count == 0) {
            	projectbasemsg.setReverse1("待审核");
            	projectbasemsg.setId(UUID.randomUUID().toString().replaceAll("-", ""));
            	projectbasemsg.setRegionalManager(!Strings.isNullOrEmpty(request.getHeader("user")) ? request.getHeader("user") : "XXX");
            }
            projectbasemsg.setCreateTime(LocalDateTime.now());
            projectbasemsg.setLoginDate(LocalDateTime.now());
            projectbasemsgService.saveOrUpdate(projectbasemsg);

            //判断类型.修改的时候可能没有项目类型字段，所以重新查询
            Projectbasemsg newProjectbasemsg = projectbasemsgService.getOne(new QueryWrapper<Projectbasemsg>().eq("ProjectNumber", projectbasemsg.getProjectNumber()));
            String projectType = !Strings.isNullOrEmpty(newProjectbasemsg.getProjectType()) ? newProjectbasemsg.getProjectType() : "";
            if (projectInfo.containsKey("ProjectType")) {
                String newProjectType = projectInfo.get("ProjectType").toString();
                if ("家用".equals(newProjectType))
                    newProjectType = "pt02";
                else
                    newProjectType = "pt01";
                if (!newProjectType.equals(projectType)) {
                    //改状态然后赋值
                    if ("pt02".equals(projectType)) {
                        Projectfamilymsg projectfamilymsg = new Projectfamilymsg();
                        projectfamilymsg.setStates("0");
                        projectfamilymsgService.update(projectfamilymsg, new QueryWrapper<Projectfamilymsg>().eq("ProjectNumber", projectbasemsg.getProjectNumber()));
                    } else {
                        Projectcommercialmsg projectcommercialmsg = new Projectcommercialmsg();
                        projectcommercialmsg.setStates("0");
                        projectcommercialmsgService.update(projectcommercialmsg, new QueryWrapper<Projectcommercialmsg>().eq("ProjectNumber", projectbasemsg.getProjectNumber()));
                    }
                    projectType = newProjectType;
                }
            }

            switch (projectType) {
                case "pt02":
                    Projectfamilymsg projectfamilymsg = jsonObject.getObject("projectInfo", Projectfamilymsg.class);
                    projectfamilymsg.setStates("1");
                    projectfamilymsg.setProjectType("pt02");
                    projectfamilymsg.setProjectDesc("家用");
                    //ht01:一室一厅 ht02:两室一厅 ht03:三室一厅 ht04:四室一厅 ht05:三室两厅 ht06:四室两厅
                    switch (!Strings.isNullOrEmpty(projectfamilymsg.getHouseType()) ? projectfamilymsg.getHouseType() : "") {
                        case "一室一厅":
                            projectfamilymsg.setHouseType("ht01");
                            break;
                        case "两室一厅":
                            projectfamilymsg.setHouseType("ht02");
                            break;
                        case "三室一厅":
                            projectfamilymsg.setHouseType("ht03");
                            break;
                        case "四室一厅":
                            projectfamilymsg.setHouseType("ht04");
                            break;
                        case "三室两厅":
                            projectfamilymsg.setHouseType("ht05");
                            break;
                        case "四室两厅":
                            projectfamilymsg.setHouseType("ht06");
                            break;
                    }

                    String reverse1_family = projectInfo.getOrDefault("Reverse1_family", "").toString();
                    String reverse2_family = projectInfo.getOrDefault("Reverse2_family", "").toString();
                    projectfamilymsg.setReverse1(reverse1_family);
                    projectfamilymsg.setReverse2(reverse2_family);
                    if (Strings.isNullOrEmpty(projectfamilymsg.getId())) {
                        projectfamilymsg.setId(UUID.randomUUID().toString().replaceAll("-", ""));
                        projectfamilymsg.setCreateTime(LocalDateTime.now());
                    } else {
                        projectfamilymsg.setUpdateTime(LocalDateTime.now());
                    }
                    projectfamilymsgService.saveOrUpdate(projectfamilymsg);
                    break;
                case "pt01":
                    Projectcommercialmsg projectcommercialmsg = jsonObject.getObject("projectInfo", Projectcommercialmsg.class);
                    if (Strings.isNullOrEmpty(projectcommercialmsg.getId())) {
                        projectcommercialmsg.setCreateTime(LocalDateTime.now());
                        projectcommercialmsg.setId(UUID.randomUUID().toString().replaceAll("-", ""));
                    } else {
                        projectcommercialmsg.setUpdateTime(LocalDateTime.now());
                    }
                    projectcommercialmsg.setStates("1");
                    projectcommercialmsg.setProjectType("pt01");
                    projectcommercialmsg.setProjectDesc("商用");
                    if ("内部议标".equals(projectcommercialmsg.getTenderForm()))
                        projectcommercialmsg.setTenderForm("tf01");
                    else if ("公开招标".equals(projectcommercialmsg.getTenderForm()))
                        projectcommercialmsg.setTenderForm("tf02");
                    //ps01:了解信息 ps02:确认经销商 ps03:方案报价 ps04:邀请招标 ps05:投标询价 ps06:价格竞争 ps07:商务谈判 ps08:合同准备 ps09:收到定金 ps10:中标 ps11:丢标
                    switch (!Strings.isNullOrEmpty(projectcommercialmsg.getProjectSchedule()) ? projectcommercialmsg.getProjectSchedule() : "") {
                        case "了解信息":
                            projectcommercialmsg.setProjectSchedule("ps01");
                            break;
                        case "确认经销商":
                            projectcommercialmsg.setProjectSchedule("ps02");
                            break;
                        case "方案报价":
                            projectcommercialmsg.setProjectSchedule("ps03");
                            break;
                        case "邀请招标":
                            projectcommercialmsg.setProjectSchedule("ps04");
                            break;
                        case "投标询价":
                            projectcommercialmsg.setProjectSchedule("ps05");
                            break;
                        case "价格竞争":
                            projectcommercialmsg.setProjectSchedule("ps06");
                            break;
                        case "商务谈判":
                            projectcommercialmsg.setProjectSchedule("ps07");
                            break;
                        case "合同准备":
                            projectcommercialmsg.setProjectSchedule("ps08");
                            break;
                        case "收到定金":
                            projectcommercialmsg.setProjectSchedule("ps09");
                            break;
                        case "中标":
                            projectcommercialmsg.setProjectSchedule("ps10");
                            break;
                        case "丢标":
                            projectcommercialmsg.setProjectSchedule("ps11");
                            break;
                        default:
                            break;
                    }

                    String reverse1_commercial = projectInfo.getOrDefault("Reverse1_commercial", "").toString();
                    projectcommercialmsg.setReverse1(reverse1_commercial);
                    projectcommercialmsg.setCreateTime(LocalDateTime.now());
                    projectcommercialmsgService.saveOrUpdate(projectcommercialmsg);
                default:
                    break;
            }
            //写入模型和数量
            List<HashMap<String, Object>> modelNumberList = (List) projectInfo.getOrDefault("ModelNumber", new ArrayList<>());
            if (modelNumberList.size() != 0) {
                parameterService.remove(new QueryWrapper<Parameter>().eq("ProjectNumber", projectbasemsg.getProjectNumber()));
            }
            modelNumberList.forEach(modelMap -> {
                Parameter parameter = new Parameter();
                parameter.setId(UUID.randomUUID().toString().replaceAll("-", ""));
                parameter.setProjectnumber(projectbasemsg.getProjectNumber());
                parameter.setModelnumber((String) modelMap.getOrDefault("model", ""));
                String number = modelMap.getOrDefault("number", "0").toString();
                parameter.setQuantity(new BigDecimal(number));
                parameter.setStates("1");
                parameter.setCreatetime(LocalDateTime.now());
                parameterService.save(parameter);
            });
            log.info("写入成功");
            state = "00000000";
            message = "成功";
        } catch (Exception e) {
            log.error("写入失败");
            e.printStackTrace();
            state = "99999999";
            message = "失败";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        returnMsg.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message, "transno", System.currentTimeMillis()));
        return returnMsg;
    }

    @PostMapping(value = "deleteProjectById")
    @ResponseBody
    @ApiOperation(value = "根据ID删除项目信息管理数据")
    public JSONObject deleteProjectById(@RequestBody JSONObject jsonObject) {
        JSONObject json = new JSONObject();
        try {
            String id = jsonObject.getString("id");
            json = projectbasemsgService.deleteProjectById(id);
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof ServiceException) {
                json.put("RespInfo", ImmutableMap.of("errorCode", ((ServiceException) e).getErrCode(), "errorMessage", ((ServiceException) e).getErrMsg()));
            } else {
                json.put("RespInfo", ImmutableMap.of("errorCode", "99999999", "errorMessage", "数据操作失败"));
            }
        }
        return json;
    }

    @PostMapping("/queryProjectmsg")
    @Transactional(rollbackFor = {Exception.class})
    @SuppressWarnings("unchecked")
    public JSONObject queryProjectfamilymsg(@RequestBody JSONObject jsonObject, HttpServletRequest request) {
        log.info("项目信息管理查询接口queryProjectmsg入参|" + jsonObject);
        JSONObject returnMsg = new JSONObject();
        String state, message;
        Date startDate = null, endDate = null;
        List<LinkedHashMap<String, Object>> linkedHashMaps;

        log.info("项目信息管理查询接口queryProjectmsg-用户信息：" + request.getHeader("user"));
        try {
            Map<String, Object> queryInfo = jsonObject.containsKey("projectInfo") ? jsonObject.getObject("projectInfo", Map.class) : new HashMap<>();

            //登录时间的区间过滤
            String loginDate = (String) queryInfo.getOrDefault("LoginDate", "");

            if (!Strings.isNullOrEmpty(loginDate)) {
                if (loginDate.startsWith("~")) {
                    startDate = new SimpleDateFormat("yyyy-MM-dd").parse("1888-01-01");
                    endDate = new SimpleDateFormat("yyyy-MM-dd").parse(loginDate.split("~")[1]);
                } else if (loginDate.split("~").length == 1) {
                    startDate = new SimpleDateFormat("yyyy-MM-dd").parse(loginDate.split("~")[0]);
                    endDate = new Date();
                } else {
                    startDate = new SimpleDateFormat("yyyy-MM-dd").parse(loginDate.split("~")[0]);
                    endDate = new SimpleDateFormat("yyyy-MM-dd").parse(loginDate.split("~")[1]);
                }
            }
            String Reverse1 = (String) queryInfo.getOrDefault("Reverse1", "");
            String ProjectAddress = (String) queryInfo.getOrDefault("ProjectAddress", "");
            String AgencyName = (String) queryInfo.getOrDefault("AgencyName", "");
            String CompoundName = (String) queryInfo.getOrDefault("CompoundName", "");

            //判断家用或者商用
            String ProjectType = (String) queryInfo.getOrDefault("ProjectName", "");


            List<AgencyMsg> agencyMsgs = userService.selectAgencyNameByUserName(request.getHeader("user"));
//            List<AgencyMsg> agencyMsgs = userService.selectAgencyNameByUserName("admin");
            List<String> agencyNames = new ArrayList<>();
            //如果没传直接获取经销商名称
            if ("".equals(AgencyName) || null == AgencyName) {
                if (!"admin".equals(request.getHeader("user"))) {
//                	if (!"admin".equals("admin")) {
                    for (AgencyMsg agencyMsg : agencyMsgs) {
                        agencyNames.add(agencyMsg.getAgencyName());
                    }
                }
            } else {
                if ("admin".equals(request.getHeader("user"))) {
//                if ("admin".equals("admin")) {
                    agencyNames.add(AgencyName);
                } else {
                      agencyNames.add(AgencyName);
                }
            }
            switch (ProjectType) {
                case "家用":
                    ProjectType = "pt02";
                    linkedHashMaps = projectbasemsgService.selectProjectFammsgList(Reverse1, ProjectType, ProjectAddress, AgencyName, CompoundName, startDate, endDate, agencyNames);
                    break;
                case "商用":
                    ProjectType = "pt01";
                    linkedHashMaps = this.projectbasemsgService.selectProjectCommsgList(Reverse1, ProjectType, ProjectAddress, AgencyName, CompoundName, startDate, endDate, agencyNames);
                    break;
                default:
                    ProjectType = "pt02";
                    linkedHashMaps = projectbasemsgService.selectProjectFammsgList(Reverse1, ProjectType, ProjectAddress, AgencyName, CompoundName, startDate, endDate, agencyNames);
                    ProjectType = "pt01";
                    linkedHashMaps.addAll(this.projectbasemsgService.selectProjectCommsgList(Reverse1, ProjectType, ProjectAddress, AgencyName, CompoundName, startDate, endDate, agencyNames));
                    break;

            }
            //遍历数据转义
            linkedHashMaps.forEach(linkedHashMap -> {
                //获取模型信息
                List<Parameter> parameterList = parameterService.list(new QueryWrapper<Parameter>().eq("ProjectNumber", linkedHashMap.getOrDefault("ProjectNumber", "")));
                List<Map<String, Object>> modelList = new ArrayList<>();
                for (Parameter parameter : parameterList) {
                    Map<String, Object> modelInfo = new HashMap<>();
                    modelInfo.put("model", parameter.getModelnumber());
                    modelInfo.put("number", parameter.getQuantity());
                    modelList.add(modelInfo);
                }
                if ("admin".equals(request.getHeader("user"))) {
                		 linkedHashMap.put("flag", "true");
                }else {
                	 String Reverse111 = (String) linkedHashMap.getOrDefault("Reverse1", "");
                	if("审核通过".equals(Reverse111)) {
                		linkedHashMap.put("flag", "false");
                	}else {
                		linkedHashMap.put("flag", "true");
                	}
                }
                linkedHashMap.put("ModelNumber", modelList);
                //家用信息转换
                switch ((String) linkedHashMap.getOrDefault("HouseType", "")) {
                    case "ht01":
                        linkedHashMap.put("HouseType", "一室一厅");
                        break;
                    case "ht02":
                        linkedHashMap.put("HouseType", "两室一厅");
                        break;
                    case "ht03":
                        linkedHashMap.put("HouseType", "三室一厅");
                        break;
                    case "ht04":
                        linkedHashMap.put("HouseType", "四室一厅");
                        break;
                    case "ht05":
                        linkedHashMap.put("HouseType", "三室两厅");
                        break;
                    case "ht06":
                        linkedHashMap.put("HouseType", "四室两厅");
                        break;
                    default:
                        break;
                }
                //小区信息
                String compoundName = (String) linkedHashMap.getOrDefault("CompoundName", "");//eg:金域蓝湾17-1-102
                if (!Strings.isNullOrEmpty(compoundName)) {
                    String[] split = compoundName.split("-");
                    String compound = split.length > 0 ? split[0] : "";
                    String CompoundName1, CompoundName2;
                    if (!Strings.isNullOrEmpty(compound) && compound.substring(compound.length() - 1).matches("[0-9]+")) {
                        if (compound.substring(compound.length() - 2, compound.length() - 1).matches("[0-9]+")) {
                            CompoundName1 = compound.substring(0, compound.length() - 2);
                            CompoundName2 = compound.substring(compound.length() - 2);
                        } else {
                            CompoundName1 = compound.substring(0, compound.length() - 1);
                            CompoundName2 = compound.substring(compound.length() - 1);
                        }
                    } else {
                        CompoundName1 = compound;
                        CompoundName2 = "";
                    }
                    String CompoundName3 = split.length > 1 ? split[1] : "";
                    String CompoundName4 = split.length > 2 ? split[2] : "";
                    linkedHashMap.put("CompoundName1", CompoundName1);
                    linkedHashMap.put("CompoundName2", CompoundName2);
                    linkedHashMap.put("CompoundName3", CompoundName3);
                    linkedHashMap.put("CompoundName4", CompoundName4);
                }
                //商用信息转换
                if ("tf01".equals(linkedHashMap.getOrDefault("TenderForm", "")))
                    linkedHashMap.put("TenderForm", "内部议标");
                else if ("tf02".equals(linkedHashMap.getOrDefault("TenderForm", "")))
                    linkedHashMap.put("TenderForm", "公开招标");
                //ps01:了解信息 ps02:确认经销商 ps03:方案报价 ps04:邀请招标 ps05:投标询价 ps06:价格竞争 ps07:商务谈判 ps08:合同准备 ps09:收到定金 ps10:中标 ps11:丢标
                switch ((String) linkedHashMap.getOrDefault("ProjectSchedule", "")) {
                    case "ps01":
                        linkedHashMap.put("ProjectSchedule", "了解信息");
                        break;
                    case "ps02":
                        linkedHashMap.put("ProjectSchedule", "确认经销商");
                        break;
                    case "ps03":
                        linkedHashMap.put("ProjectSchedule", "方案报价");
                        break;
                    case "ps04":
                        linkedHashMap.put("ProjectSchedule", "邀请招标");
                        break;
                    case "ps05":
                        linkedHashMap.put("ProjectSchedule", "投标询价");
                        break;
                    case "ps06":
                        linkedHashMap.put("ProjectSchedule", "价格竞争");
                        break;
                    case "ps07":
                        linkedHashMap.put("ProjectSchedule", "商务谈判");
                        break;
                    case "ps08":
                        linkedHashMap.put("ProjectSchedule", "合同准备");
                        break;
                    case "ps09":
                        linkedHashMap.put("ProjectSchedule", "收到定金");
                        break;
                    case "ps10":
                        linkedHashMap.put("ProjectSchedule", "中标");
                        break;
                    case "ps11":
                        linkedHashMap.put("ProjectSchedule", "丢标");
                        break;
                    default:
                        break;
                }

                //通用信息转换
                if ("pt02".equals(linkedHashMap.getOrDefault("ProjectType", ""))) {
                    linkedHashMap.put("ProjectType", "家用");
                } else if ("pt01".equals(linkedHashMap.getOrDefault("ProjectType", ""))) {
                    linkedHashMap.put("ProjectType", "商用");
                }
                if ("cg1".equals(linkedHashMap.getOrDefault("Reverse3_pbm", ""))) {
                    linkedHashMap.put("Reverse3_pbm", "成功");
                } else if ("cg2".equals(linkedHashMap.getOrDefault("Reverse3_pbm", ""))) {
                    linkedHashMap.put("Reverse3_pbm", "失败");
                }
                if ("po01".equals(linkedHashMap.getOrDefault("ProjectOrigin", ""))) {
                    linkedHashMap.put("ProjectOrigin", "个人");
                } else if ("po02".equals(linkedHashMap.getOrDefault("ProjectOrigin", ""))) {
                    linkedHashMap.put("ProjectOrigin", "其他");
                }
                if ("01".equals(linkedHashMap.getOrDefault("IfCrossArea", "")))
                    linkedHashMap.put("IfCrossArea", "是");
                else if ("02".equals(linkedHashMap.getOrDefault("IfCrossArea", "")))
                    linkedHashMap.put("IfCrossArea", "否");

                //psm01:商用多联 psm02:模块系列 psm03:户型机系列 psm04:风管机系列 psm05:终端设备系列 psm06:热水机系列 psm07:其他
                List<String> preSelectedModelList = new ArrayList<>();
                for (String preSelectedModel : ((String) linkedHashMap.getOrDefault("PreSelectedModel", "")).split(",")) {
                    switch ((preSelectedModel)) {
                        case "psm01":
                            preSelectedModelList.add("商用多联");
                            break;
                        case "psm02":
                            preSelectedModelList.add("模块系列");
                            break;
                        case "psm03":
                            preSelectedModelList.add("户式水机系列");
                            break;
                        case "psm04":
                            preSelectedModelList.add("风管机系列");
                            break;
                        case "psm05":
                            preSelectedModelList.add("终端设备系列");
                            break;
                        case "psm06":
                            preSelectedModelList.add("热水机系列");
                            break;
                        case "psm07":
                            preSelectedModelList.add("其他");
                            break;
                        case "psm08":
                            preSelectedModelList.add("家用多联");
                            break;
                        default:
                            break;
                    }
                }
                linkedHashMap.put("PreSelectedModel", preSelectedModelList);
                //sr01:30% sr02:50% sr03:80% sr04:95% sr05:100%
                switch ((String) linkedHashMap.getOrDefault("SuccessRate", "")) {
                    case "sr01":
                        linkedHashMap.put("SuccessRate", "30%");
                        break;
                    case "sr02":
                        linkedHashMap.put("SuccessRate", "50%");
                        break;
                    case "sr03":
                        linkedHashMap.put("SuccessRate", "80%");
                        break;
                    case "sr04":
                        linkedHashMap.put("SuccessRate", "95%");
                        break;
                    case "sr05":
                        linkedHashMap.put("SuccessRate", "100%");
                        break;
                    default:
                        break;
                }
            });

            //查询的偏移量和总量
            PageInfo pageInfo = jsonObject.getObject("pageInfo", PageInfo.class);
            //总条数
            pageInfo.setTotalCount(Long.parseLong(linkedHashMaps.size() + ""));
            //共多少页
            pageInfo.setTotalPage((int) Math.floor((double) pageInfo.getTotalCount() / pageInfo.getPageSize()) + 1);
            if (pageInfo.getTotalPage() < pageInfo.getCurrentPage() && pageInfo.getTotalCount() != 0) {
                returnMsg.put("RespInfo", ImmutableMap.of("errorCode", "02020202", "errorMessage", "请求页码错误，超出总页码", "transno", System.currentTimeMillis()));
                log.info("项目信息管理查询接口queryProjectmsg出参|" + returnMsg);
                return returnMsg;
            }
            returnMsg.put("pageInfo", pageInfo);

            int end = pageInfo.getCurrentPage() * pageInfo.getPageSize();
            List<LinkedHashMap<String, Object>> pageList = linkedHashMaps.subList(((pageInfo.getCurrentPage() - 1) * pageInfo.getPageSize()), (end > linkedHashMaps.size() ? linkedHashMaps.size() : end));
            returnMsg.put("queryInfoList", pageList);
            log.info("查询成功");
            state = "00000000";
            message = "成功";
        } catch (Exception e) {
            log.error("查询失败");
            e.printStackTrace();
            state = "99999999";
            message = "失败";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        returnMsg.put("RespInfo", ImmutableMap.of("errorCode", state, "errorMessage", message, "transno", System.currentTimeMillis()));
        log.info("项目信息管理查询接口queryProjectmsg出参|" + returnMsg);
        return returnMsg;
    }

}
