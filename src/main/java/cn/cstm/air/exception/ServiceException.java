package cn.cstm.air.exception;

public class ServiceException extends Exception {
	private static final long serialVersionUID = 1L;
	/**  异常代码*/
	private String errCode;
	/**  异常描述*/
	private String errMsg;
	
	public ServiceException(String errCode,String errMsg){
		this.errCode = errCode;
		this.errMsg = errMsg;
	}

	public String getErrCode() {
		return errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}
}

