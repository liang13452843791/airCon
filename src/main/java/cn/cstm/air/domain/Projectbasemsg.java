package cn.cstm.air.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author author
 * @since 2020-04-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Projectbasemsg implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("ProjectNumber")
    private String ProjectNumber;

    @TableField("id")
    private String id;

	@TableField("ProjectOrigin")
	private String ProjectOrigin;

    @TableField("ProjectLeader")
    private String ProjectLeader;

    @TableField("TeleNumber")
    private String TeleNumber;

    @TableField("CalibrationDate")
    private LocalDateTime CalibrationDate;

    @TableField("IfCrossArea")
    private String IfCrossArea;

    @TableField("TakeDeliveryDate")
    private LocalDateTime TakeDeliveryDate;

    @TableField("CompletionDate")
    private LocalDateTime CompletionDate;

    @TableField("PreSelectedModel")
    private String PreSelectedModel;

    @TableField("SuccessRate")
    private String SuccessRate;

    @TableField("FuturePrices")
    private Double FuturePrices;

    @TableField("ProjectType")
    private String ProjectType;

    @TableField("ProjectAddress")
    private String ProjectAddress;

    @TableField("LoginDate")
    private LocalDateTime LoginDate;

    @TableField("RegionalManager")
    private String RegionalManager;

    @TableField("StructureArea")
    private Double StructureArea;

    @TableField("States")
    private String States;

    @TableField("CreateTime")
    private LocalDateTime CreateTime;

    @TableField("UpdateTime")
    private LocalDateTime UpdateTime;

    @TableField("AgencyName")
    private String AgencyName;

    @TableField("Reverse2")
    private String Reverse2;

    @TableField("Reverse3")
    private String Reverse3;

    @TableField("Reverse4")
    private String Reverse4;

    @TableField("Reverse5")
    private String Reverse5;

    @TableField("Reverse1")
    private String Reverse1;

	public String getProjectNumber() {
		return ProjectNumber;
	}

	public void setProjectNumber(String projectNumber) {
		ProjectNumber = projectNumber;
	}

	public String getProjectOrigin() {
		return ProjectOrigin;
	}

	public void setProjectOrigin(String projectOrigin) {
		ProjectOrigin = projectOrigin;
	}

	public String getProjectLeader() {
		return ProjectLeader;
	}

	public void setProjectLeader(String projectLeader) {
		ProjectLeader = projectLeader;
	}

	public String getTeleNumber() {
		return TeleNumber;
	}

	public void setTeleNumber(String teleNumber) {
		TeleNumber = teleNumber;
	}

	public LocalDateTime getCalibrationDate() {
		return CalibrationDate;
	}

	public void setCalibrationDate(LocalDateTime calibrationDate) {
		CalibrationDate = calibrationDate;
	}

	public String getIfCrossArea() {
		return IfCrossArea;
	}

	public void setIfCrossArea(String ifCrossArea) {
		IfCrossArea = ifCrossArea;
	}

	public LocalDateTime getTakeDeliveryDate() {
		return TakeDeliveryDate;
	}

	public void setTakeDeliveryDate(LocalDateTime takeDeliveryDate) {
		TakeDeliveryDate = takeDeliveryDate;
	}

	public LocalDateTime getCompletionDate() {
		return CompletionDate;
	}

	public void setCompletionDate(LocalDateTime completionDate) {
		CompletionDate = completionDate;
	}

	public String getPreSelectedModel() {
		return PreSelectedModel;
	}

	public void setPreSelectedModel(String preSelectedModel) {
		PreSelectedModel = preSelectedModel;
	}

	public String getSuccessRate() {
		return SuccessRate;
	}

	public void setSuccessRate(String successRate) {
		SuccessRate = successRate;
	}

	public Double getFuturePrices() {
		return FuturePrices;
	}

	public void setFuturePrices(Double futurePrices) {
		FuturePrices = futurePrices;
	}

	public String getProjectType() {
		return ProjectType;
	}

	public void setProjectType(String projectType) {
		ProjectType = projectType;
	}

	public String getProjectAddress() {
		return ProjectAddress;
	}

	public void setProjectAddress(String projectAddress) {
		ProjectAddress = projectAddress;
	}

	public LocalDateTime getLoginDate() {
		return LoginDate;
	}

	public void setLoginDate(LocalDateTime loginDate) {
		LoginDate = loginDate;
	}

	public String getRegionalManager() {
		return RegionalManager;
	}

	public void setRegionalManager(String regionalManager) {
		RegionalManager = regionalManager;
	}

	public Double getStructureArea() {
		return StructureArea;
	}

	public void setStructureArea(Double structureArea) {
		StructureArea = structureArea;
	}

	public String getStates() {
		return States;
	}

	public void setStates(String states) {
		States = states;
	}

	public LocalDateTime getCreateTime() {
		return CreateTime;
	}

	public void setCreateTime(LocalDateTime createTime) {
		CreateTime = createTime;
	}

	public LocalDateTime getUpdateTime() {
		return UpdateTime;
	}

	public void setUpdateTime(LocalDateTime updateTime) {
		UpdateTime = updateTime;
	}

	public String getAgencyName() {
		return AgencyName;
	}

	public void setAgencyName(String agencyName) {
		AgencyName = agencyName;
	}

	public String getReverse2() {
		return Reverse2;
	}

	public void setReverse2(String reverse2) {
		Reverse2 = reverse2;
	}

	public String getReverse3() {
		return Reverse3;
	}

	public void setReverse3(String reverse3) {
		Reverse3 = reverse3;
	}

	public String getReverse4() {
		return Reverse4;
	}

	public void setReverse4(String reverse4) {
		Reverse4 = reverse4;
	}

	public String getReverse5() {
		return Reverse5;
	}

	public void setReverse5(String reverse5) {
		Reverse5 = reverse5;
	}

	public String getReverse1() {
		return Reverse1;
	}

	public void setReverse1(String reverse1) {
		Reverse1 = reverse1;
	}


}
