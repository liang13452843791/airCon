package cn.cstm.air.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author author
 * @since 2020-04-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Projectcommercialmsg implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("id")
    private String id;

    @TableField("ProjectType")
    private String ProjectType;

    @TableField("ProjectDesc")
    private String ProjectDesc;

    @TableField("ProjectName")
    private String ProjectName;

    @TableField("TenderForm")
    private String TenderForm;

    @TableField("DecisionMaker")
    private String DecisionMaker;

    @TableField("DecisionMakerPhone")
    private String DecisionMakerPhone;

    @TableField("ProjectSchedule")
    private String ProjectSchedule;

    @TableField("ProjectUse")
    private String ProjectUse;

    @TableField("States")
    private String States;

    @TableField("CreateTime")
    private LocalDateTime CreateTime;

    @TableField("UpdateTime")
    private LocalDateTime UpdateTime;

    @TableField("Reverse1")
    private String Reverse1;

    @TableField("Reverse2")
    private String Reverse2;

    @TableField("Reverse3")
    private String Reverse3;

    @TableField("Reverse4")
    private String Reverse4;

    @TableField("Reverse5")
    private String Reverse5;

    @TableId("ProjectNumber")
    private String ProjectNumber;


}
