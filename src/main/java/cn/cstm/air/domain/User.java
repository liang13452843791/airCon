package cn.cstm.air.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;

@TableName("user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String UserName;
    private String PassWord;
    private String UserDesc;
    private String TelePhone;
    private String Email;
    private Date createtime;
    private Date updatetime;
    private String status;
    private String Reverse1;
    private String Reverse2;
    private String Reverse3;
    private String Reverse4;
    private String Reverse5;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getPassWord() {
		return PassWord;
	}
	public void setPassWord(String passWord) {
		PassWord = passWord;
	}
	public String getUserDesc() {
		return UserDesc;
	}
	public void setUserDesc(String userDesc) {
		UserDesc = userDesc;
	}
	public String getTelePhone() {
		return TelePhone;
	}
	public void setTelePhone(String telePhone) {
		TelePhone = telePhone;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getReverse1() {
		return Reverse1;
	}
	public void setReverse1(String reverse1) {
		Reverse1 = reverse1;
	}
	public String getReverse2() {
		return Reverse2;
	}
	public void setReverse2(String reverse2) {
		Reverse2 = reverse2;
	}
	public String getReverse3() {
		return Reverse3;
	}
	public void setReverse3(String reverse3) {
		Reverse3 = reverse3;
	}
	public String getReverse4() {
		return Reverse4;
	}
	public void setReverse4(String reverse4) {
		Reverse4 = reverse4;
	}
	public String getReverse5() {
		return Reverse5;
	}
	public void setReverse5(String reverse5) {
		Reverse5 = reverse5;
	}
	
	public User(String id, String userName, String passWord, String userDesc, String telePhone, String email,
			Date createtime, Date updatetime, String status, String reverse1, String reverse2, String reverse3,
			String reverse4, String reverse5) {
		super();
		this.id = id;
		UserName = userName;
		PassWord = passWord;
		UserDesc = userDesc;
		TelePhone = telePhone;
		Email = email;
		this.createtime = createtime;
		this.updatetime = updatetime;
		this.status = status;
		Reverse1 = reverse1;
		Reverse2 = reverse2;
		Reverse3 = reverse3;
		Reverse4 = reverse4;
		Reverse5 = reverse5;
	}
	public User() {
		super();
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", UserName=" + UserName + ", PassWord=" + PassWord + ", UserDesc=" + UserDesc
				+ ", TelePhone=" + TelePhone + ", Email=" + Email + ", createtime=" + createtime + ", updatetime="
				+ updatetime + ", status=" + status + ", Reverse1=" + Reverse1 + ", Reverse2=" + Reverse2
				+ ", Reverse3=" + Reverse3 + ", Reverse4=" + Reverse4 + ", Reverse5=" + Reverse5 + "]";
	}
	
}
