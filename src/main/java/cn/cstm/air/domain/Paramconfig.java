package cn.cstm.air.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author author
 * @since 2020-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Paramconfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("ParamType")
    private String ParamType;

    @TableField("ParamKey")
    private String ParamKey;

    @TableField("ParamValue")
    private String ParamValue;

    @TableField("ParamDesc")
    private String ParamDesc;

    @TableField("CreateTime")
    private LocalDateTime CreateTime;

    @TableField("UpdateTime")
    private LocalDateTime UpdateTime;

    @TableField("Status")
    private String Status;

    @TableField("Reverse1")
    private String Reverse1;

    @TableField("Reverse2")
    private String Reverse2;

    @TableField("Reverse3")
    private String Reverse3;

    @TableField("Reverse4")
    private String Reverse4;

    @TableField("Reverse5")
    private String Reverse5;


}
