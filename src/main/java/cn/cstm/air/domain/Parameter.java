package cn.cstm.air.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author author
 * @since 2020-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Parameter implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("id")
    private String id;

    @TableField("projectnumber")
    private String projectnumber;

    @TableField("modelnumber")
    private String modelnumber;

    @TableField("quantity")
    private BigDecimal quantity;

    @TableField("states")
    private String states;

    @TableField("createtime")
    private LocalDateTime createtime;

    @TableField("updatetime")
    private LocalDateTime updatetime;

    @TableField("reverse1")
    private String reverse1;

    @TableField("reverse2")
    private String reverse2;

    @TableField("reverse3")
    private String reverse3;

    @TableField("reverse4")
    private String reverse4;

    @TableField("reverse5")
    private String reverse5;


}
