package cn.cstm.air.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author author
 * @since 2020-04-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Userandrole implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("UserName")
    private String UserName;

    @TableField("RoleID")
    private String RoleID;

    @TableField("RoleName")
    private String RoleName;

    @TableField("CreateTime")
    private Date CreateTime;

    @TableField("UpdateTime")
    private Date UpdateTime;

    @TableField("Status")
    private String Status;

    @TableField("Reverse1")
    private String Reverse1;

    @TableField("Reverse2")
    private String Reverse2;

    @TableField("Reverse3")
    private String Reverse3;

    @TableField("Reverse4")
    private String Reverse4;

    @TableField("Reverse5")
    private String Reverse5;

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getRoleID() {
		return RoleID;
	}

	public void setRoleID(String roleID) {
		RoleID = roleID;
	}

	public String getRoleName() {
		return RoleName;
	}

	public void setRoleName(String roleName) {
		RoleName = roleName;
	}


	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getReverse1() {
		return Reverse1;
	}

	public void setReverse1(String reverse1) {
		Reverse1 = reverse1;
	}

	public String getReverse2() {
		return Reverse2;
	}

	public void setReverse2(String reverse2) {
		Reverse2 = reverse2;
	}

	public String getReverse3() {
		return Reverse3;
	}

	public void setReverse3(String reverse3) {
		Reverse3 = reverse3;
	}

	public String getReverse4() {
		return Reverse4;
	}

	public void setReverse4(String reverse4) {
		Reverse4 = reverse4;
	}

	public String getReverse5() {
		return Reverse5;
	}

	public void setReverse5(String reverse5) {
		Reverse5 = reverse5;
	}


}
