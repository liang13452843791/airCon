package cn.cstm.air.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author author
 * @since 2020-04-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Projectfamilymsg implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("id")
    private String id;

    @TableField("ProjectType")
    private String ProjectType;

    @TableField("ProjectDesc")
    private String ProjectDesc;

    @TableField("CompoundName")
    private String CompoundName;

    @TableField("OwnerName")
    private String OwnerName;

    @TableField("UsableArea")
    private Double UsableArea;

    @TableField("HouseType")
    private String HouseType;

    @TableField("States")
    private String States;

    @TableField("CreateTime")
    private LocalDateTime CreateTime;

    @TableField("UpdateTime")
    private LocalDateTime UpdateTime;

    @TableField("Reverse1")
    private String Reverse1;

    @TableField("Reverse2")
    private String Reverse2;

    @TableField("Reverse3")
    private String Reverse3;

    @TableField("Reverse4")
    private String Reverse4;

    @TableField("Reverse5")
    private String Reverse5;

    @TableId("ProjectNumber")
    private String ProjectNumber;


}
