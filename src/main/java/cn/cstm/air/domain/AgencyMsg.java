package cn.cstm.air.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;

@TableName("AgencyMsg")
public class AgencyMsg implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String UserName;
    private String AgencyName;
    private String AgencyPhone;
    private String LoginUserName;
    private String LoginUserPhone;
    private String States;
    private Date CreateTime;
    private Date UpdateTime;
    private String status;
    private String Reverse1;
    private String Reverse2;
    private String Reverse3;
    private String Reverse4;
    private String Reverse5;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getAgencyName() {
		return AgencyName;
	}
	public void setAgencyName(String agencyName) {
		AgencyName = agencyName;
	}
	public String getAgencyPhone() {
		return AgencyPhone;
	}
	public void setAgencyPhone(String agencyPhone) {
		AgencyPhone = agencyPhone;
	}
	public String getLoginUserName() {
		return LoginUserName;
	}
	public void setLoginUserName(String loginUserName) {
		LoginUserName = loginUserName;
	}
	public String getLoginUserPhone() {
		return LoginUserPhone;
	}
	public void setLoginUserPhone(String loginUserPhone) {
		LoginUserPhone = loginUserPhone;
	}
	public String getStates() {
		return States;
	}
	public void setStates(String states) {
		States = states;
	}
	
	public Date getCreateTime() {
		return CreateTime;
	}
	public void setCreateTime(Date createTime) {
		CreateTime = createTime;
	}
	public Date getUpdateTime() {
		return UpdateTime;
	}
	public void setUpdateTime(Date updateTime) {
		UpdateTime = updateTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReverse1() {
		return Reverse1;
	}
	public void setReverse1(String reverse1) {
		Reverse1 = reverse1;
	}
	public String getReverse2() {
		return Reverse2;
	}
	public void setReverse2(String reverse2) {
		Reverse2 = reverse2;
	}
	public String getReverse3() {
		return Reverse3;
	}
	public void setReverse3(String reverse3) {
		Reverse3 = reverse3;
	}
	public String getReverse4() {
		return Reverse4;
	}
	public void setReverse4(String reverse4) {
		Reverse4 = reverse4;
	}
	public String getReverse5() {
		return Reverse5;
	}
	public void setReverse5(String reverse5) {
		Reverse5 = reverse5;
	}
	public AgencyMsg() {
		super();
	}
	public AgencyMsg(String agencyPhone,String id, String agencyName, String loginUserName, String loginUserPhone,
			Date updateTime) {
		super();
		this.id = id;
		AgencyName = agencyName;
		AgencyPhone = agencyPhone;
		LoginUserName = loginUserName;
		LoginUserPhone = loginUserPhone;
		UpdateTime = updateTime;
	}
	
}
