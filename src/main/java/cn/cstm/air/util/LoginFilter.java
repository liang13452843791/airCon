package cn.cstm.air.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@WebFilter(filterName = "loginFilter", urlPatterns = {"/*"})
public class LoginFilter implements Filter {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        String url = request.getRequestURI();
        String contextPath = request.getContextPath();
        //静态资源放行
        if (url.endsWith(".css") || url.endsWith(".js") || url.endsWith(".jpg")
                || url.endsWith(".gif") || url.endsWith(".png") || url.endsWith(".otf") || url.endsWith(".eot")
                || url.endsWith(".svg") || url.endsWith(".ttf") || url.endsWith(".woff") || url.endsWith(".woff2")
                || url.endsWith(".scss") || url.endsWith(".gif") || url.endsWith(".png")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        } else if(url.endsWith("loginAction")||url.endsWith("getCode")||url.endsWith("notLogin")||url.endsWith("loginOut")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }else  {
        	String user = request.getHeader("user");
        	logger.info("LoginFilter-用户名：" + user);
        	if(null!=user&&!"".equals(user)) {
        		filterChain.doFilter(servletRequest, servletResponse);
        	}else {
        		response.sendRedirect(contextPath+"/notLogin");
        	}
//        	User user=(User)session.getAttribute("user");
//        	if(null==user) {
//        		response.sendRedirect(contextPath+"/notLogin");
//        	}else {
//        		 filterChain.doFilter(servletRequest, servletResponse);
//        	}
        }

    }

    @Override
    public void destroy() {

    }
}
