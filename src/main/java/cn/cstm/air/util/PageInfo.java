/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package cn.cstm.air.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/***
 * @author ljp
 * @date 2020年2月15日13点32分
 */
@ApiModel(description = "查询分页信息")
@Data
public class PageInfo implements Serializable {
    @ApiModelProperty("每页显示的条数")
    private int pageSize;
    @ApiModelProperty("当前页码")
    private int currentPage;
    @ApiModelProperty("共多少页")
    private int totalPage;
    @ApiModelProperty("总条数")
    private Long totalCount;
}
